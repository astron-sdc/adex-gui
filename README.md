# ADEX-GUI

Astron Data Explorer - GUI

This frontend (ReactJS) connects to the ESAP-api-gateway (Django).

## Documentation

https://support.astron.nl/confluence/display/SDCP/ADEX+Documentation

Local deployment

```
npm install
npm start
```

## Tests

### Testing information

Jest Node-based test runner which is accompanied by Create React App. All information can also be found
here: https://create-react-app.dev/docs/running-tests/

For front-end testing shallow rendering is used (react-test-renderer). Shallow rendering is faster than mounting a
component, and allows focussing unit tests on a specific component without having to worry about what any of its
children may be doing.

#### Convention

1) Put your test class in the same folder as your class
2) Name your test class {class}.test.js
3) For every test add it() or test() blocks.
4) Group tests together, whenever logical in a describe() block.
5) Always use expect as the last statement(s): https://jestjs.io/docs/expect
6) Use expect(fn).toBeCalled()

#### Development testing

- in watch mode:

```
  npm test
```

- all tests once:

```
npm run t-once
```

- check test coverage for current changes:

``` 
- npm run t-cov
```

- check total test coverage:


``` npm run t-total```


## Releasing

A releasing tool called 'release-it' is used for versioning. See https://github.com/release-it/release-it for more information. 

When deploying the master branch, the command ```npm run release -- minor --ci``` is run and hence a minor version is released (1.0.0 -> 1.1.0)

For a major releases, patches etc. it has to be done manually via ```npm run release```
