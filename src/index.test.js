import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from "./serviceWorker";
import expect from "expect";

it('renders root (index) page without crashing', () => {
    const root = document.createElement('root');
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.findDOMNode(root);
});