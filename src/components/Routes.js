import React from "react";
import {BrowserRouter as Router, Switch} from "react-router-dom";
import HorizontalMenu from "../views/HorizontalMenu";
import {PagesCreator} from "./pages/PagesCreator";

//See implementation diagram for the structure
export default function Routes() {
    return (
        <Router basename="adex-gui">
            <HorizontalMenu/>
            <Switch>
                 <PagesCreator/>
            </Switch>
        </Router>
    );
}
