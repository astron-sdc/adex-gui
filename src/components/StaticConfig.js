import {Header} from "../models/menuOptions/Header";
import {Dropdown} from "../models/menuOptions/Dropdown";
import {InputField} from "../models/menuOptions/InputField";
import {typeStylingDict} from "../utils/inputFieldTypeStylingDict";
import {Menu} from "../models/Menu";
import {Checkbox} from "../models/menuOptions/Checkbox";
import {Page} from "../models/Page";
import QueryViewPage from "./pages/skyViews/QueryViewPage";
import ArchivesOverview from "./pages/archives/ArchivesOverview"
import React from "react";
import {RangeSlider} from "../models/menuOptions/RangeSlider";
import {MultiRow} from "../models/menuOptions/MultiRow";

// configuration for SkyView plugin

export const SkyViewHeader = new Header("SkyView", undefined, "Query", "Show selection(s) in the skyview. It refreshes the data and performs a new query", "search");
export const Survey = new Dropdown("Survey", "Background HiPS survey.", "h4", "right");
export const ColorMap = new Dropdown("Color Map", "Change the color scheme of the background survey", "h4");
export const AladinConfig = new MultiRow("Sky settings", undefined, [Survey, ColorMap])
export const Ra = new InputField("RA", undefined, typeStylingDict.number);
export const Dec = new InputField("Dec", undefined, typeStylingDict.number);
export const FoV = new InputField("FoV", undefined, typeStylingDict.number);
export const Coordinates = new MultiRow("Coordinates", "In decimal degrees", [Ra, Dec, FoV])
export const FrequencySlider = new RangeSlider("Frequency", "In megahertz (MHz)")
export const CalibrationLevel = new Checkbox("Calibration Level", "Level of calibration and processing. Raw (0), Calibrated (1) or Processed (2).")
export const DataProductType = new Checkbox("DataProduct Type", "Like images, cubes, (radio) visibilities or time domain data");
export const ArchiveSelection = new Checkbox("Archives", "Go to the archive page to find out more about our archives/telescopes.");
export const Collection = new Checkbox("Collections", "The group to which the data belongs too.")


export const SkyViewMenu = new Menu(
    SkyViewHeader,
    [AladinConfig, FrequencySlider, Coordinates],
    [DataProductType, CalibrationLevel, ArchiveSelection, Collection]
);


// configuration for Archives plugin

export const ArchiveHeader = new Header("Archives");
export const ArchivesCheckbox = new Checkbox("Archives");
export const ArchiveMenu = new Menu(
    ArchiveHeader,
    [ArchivesCheckbox],
    undefined
);


const Home = new Page("", undefined, <QueryViewPage/>, undefined)
export const SkyView = new Page("SkyView", "star", <QueryViewPage/>, SkyViewMenu)
export const Archives = new Page("Archives", "database", <ArchivesOverview/>, ArchiveMenu)

export function getPages() {
    return [Home, SkyView, Archives];
}