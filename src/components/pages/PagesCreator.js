import React, {useReducer} from "react";
import {Route} from "react-router-dom";
import {Page} from "../../models/Page";
import VerticalMenu from "../../views/VerticalMenu";
import MenuContent from "../../views/menu/MenuContent";
import PageContent from "../../views/page/PageContent";
import {
    ArchivesController
} from "./archives/ArchivesController";
import {
    SkyViewController
} from "./skyViews/SkyViewController";
import {getPages, SkyView, Archives} from "../StaticConfig";

export function PagesCreator() {
    ArchivesController();
    SkyViewController();

    const initialState = {[SkyView.name]: true, [Archives.name]: true};

    function reducer(state, pageName) {
        return {...state, [pageName]: !state[pageName]}

    }

    const [state, dispatch] = useReducer(reducer, initialState);

    function getAllPageElementsWithRoute(page) {
        return <Route key={"route-" + page.name} exact path={Page.getRoute(page)}>
            <VerticalMenu activePage={page} handleShowMenu={() => dispatch(page.name)}/>
            <main className="content-wrapper horizontal-split">
                <MenuContent menu={page.menu} showMenu={state[page.name]}/>
                <PageContent content={page.content}/>
            </main>
        </Route>;
    }

    return getPages().map(page => getAllPageElementsWithRoute(page));

}