import {useContext} from "react";
import {UIContext} from "../../../contexts/UIContext";
import {
    ArchivesCheckbox,
} from "../../StaticConfig";
import {getGroupedSetterFunction, isEmptyObject} from "../../../utils/Util";

export function ArchivesController() {
    const {
        archives, setArchives,
    } = useContext(UIContext);

    if (isEmptyObject(archives) || !archives.allArchives) {
        return;
    }

    // this binds the Archives checkbox to the changeArchive function
    ArchivesCheckbox.bindCheckbox(getGroupedSetterFunction(setArchives, "selectedArchives"), archives.archiveShortDescriptions, archives.selectedArchives);
}

