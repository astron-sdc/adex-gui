import React, { useContext } from "react";
import { Container, Row, Col } from "react-bootstrap";
import ArchiveCard from "./ArchiveCard";

import {UIContext} from "../../../contexts/UIContext";
import {isEmptyObject} from "../../../utils/Util";
import Spinner from "../../../views/basics/Spinner";

export default function ArchivesOverview() {
  const { archives } = useContext(UIContext);

    if (isEmptyObject(archives) || !archives.allArchives) {
        return <Spinner message={"Retrieving archives"}/>
    }

  return (
    <Container fluid>
      <Row>
          {archives.allArchives.map((archive) => {
              // only show selected archives
              if (! archives.selectedArchives.includes(archive.name)) { return null;}

              return <Col><ArchiveCard archive={archive} /></Col>;
          })}
      </Row>
    </Container>
  );
}
