import React, { useState, useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { GlobalContext } from "../../../contexts/GlobalContext";

import DatasetDescriptionDiasModal from './DatasetDescriptionDiasModal'

export default function DataSets({ archive }) {
  const { api_host } = useContext(GlobalContext);
  const [datasets, setDatasets] = useState([]);

  useEffect(() => {
    axios
      .get(api_host + "query/datasets-uri")
      .then((response) => setDatasets(response.data["results"]));
  }, [api_host]);
  if (!datasets) return null;

  return (

        <div className="card__content card__content--background">

        <table>
          {datasets
            .filter((dataset) => archive.datasets.includes(dataset.uri))
            .map((dataset) => {
              return (
                  <div>
                      <tr>
                          <td>
                              &nbsp;<DatasetDescriptionDiasModal dataset={dataset} title={dataset.name}
                                                                 modalClasses="flex-container--shrink"/>
                          </td>
                          <td>
                              &nbsp;{dataset.name}
                          </td>
                      </tr>
                  </div>)
            })}
        </table>

      </div>
  );
}
