import React, { useContext, useState, useEffect } from "react";
import { Button, Modal, Table, Row,Col } from "react-bootstrap";

import { getCircleInfoIcon, getDownloadIcon } from '../../../utils/styling'

export default function DatasetDescriptionModal(props) {
    const [description, setSetDescription] = useState(props.dataset.long_description);

    const showLongDescriptionClick = () => {
        setSetDescription(props.dataset.long_description)
    }

    const showRetrievalDescriptionClick = () => {
        setSetDescription(props.dataset.retrieval_description)
    }

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function close(){
        setShow(false)
    }

    let header_content = <div>
        {getCircleInfoIcon("darkblue")}{' '}{props.dataset.short_description}
    </div>

    let body_content = <div>
        <table>
            <tbody>
            <tr><td><b>Collection</b></td><td>: {props.dataset.collection}</td></tr>
            <tr><td><b>Level     </b></td><td>: {props.dataset.level}</td></tr>
            <tr><td><b>Category  </b></td><td>: {props.dataset.category}</td></tr>
            <tr>&nbsp;</tr>
            </tbody>
        </table>

        <h5>{description}</h5>
    </div>

    let footer_content = <div>
        <Button variant="outline-secondary" onClick={showLongDescriptionClick}>
            {getCircleInfoIcon("darkblue")}{' '}Description
        </Button>
        <Button variant="outline-secondary" onClick={showRetrievalDescriptionClick}>
            {getDownloadIcon("darkblue")}{' '}Retrieval Info
        </Button>
        <Button variant="outline-secondary" onClick={close}>Close</Button>
    </div>

    let myModal = <div>
        <Modal.Header>
            <Modal.Title>{header_content} </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            {body_content}
        </Modal.Body>
        <Modal.Footer>
            {footer_content}
        </Modal.Footer>
    </div>

    return (
        <>
        <Button
            type="button"
            variant="outline-secondary"
            onClick={handleShow}
            {...props}>
            {getCircleInfoIcon(props.icon_color)}
        </Button>

        <Modal size="lg" show={show} onHide={handleClose}>
            {myModal}
        </Modal>
        </>
    )


}
