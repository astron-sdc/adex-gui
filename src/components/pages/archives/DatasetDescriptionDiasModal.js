import React, {useState} from "react";
import DiasModal from "../../../views/basics/Modal";
import Button from "../../../views/basics/Button";
import Icon from "../../../views/basics/Icon";

export default function DatasetDescriptionModal(props) {

    const [description, setSetDescription] = useState(props.dataset.long_description);

    const showLongDescriptionClick = () => {
        setSetDescription(props.dataset.long_description)
    }
    const showRetrievalDescriptionClick = () => {
        setSetDescription(props.dataset.retrieval_description)
    }

    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);

    function close() {
        setShow(false)
    }

    let body_content = <>
        <table>
            <tbody>
            <tr>
                <td><b>Collection</b></td>
                <td>: {props.dataset.collection}</td>
            </tr>
            <tr>
                <td><b>Level </b></td>
                <td>: {props.dataset.level}</td>
            </tr>
            <tr>
                <td><b>Category </b></td>
                <td>: {props.dataset.category}</td>
            </tr>
            <tr>&nbsp;</tr>
            </tbody>
        </table>

        <h5>{description}</h5>
    </>

    const isActive = (fieldName, description) => props.dataset[fieldName] === description;
    const isDisabled = (fieldName) => props.dataset[fieldName] === "";

    let footer_content = <>
        <Button title={"Description"}
                handleClick={isDisabled("long_description")  ? null : showLongDescriptionClick}
                styling={["secondary", "icon-button", isActive("long_description", description) ? "secondary active" : ""]}
                isDisabled={isDisabled("long_description")}
                icon={<Icon name={"info"} styling={["medium"]}/>}/>

        <Button title={"Retrieval Info"}
                handleClick={isDisabled("retrieval_description") ? null : showRetrievalDescriptionClick}
                styling={["primary", "icon-button", isActive("retrieval_description", description) ? "secondary active" : ""]}
                isDisabled={isDisabled("retrieval_description")}
                icon={<Icon name={"info"} styling={["archive-arrow-up"]}/>}/>
    </>

    return (
        <>
            <Button handleClick={handleShow} styling={["secondary", "icon-button"]} icon={<Icon name={"info"}/>}/>
            <DiasModal
                headerIcon={"info"}
                title={props.dataset.short_description}
                content={body_content}
                footer={footer_content}
                show={show}
                onClose={close}
            />
        </>
    )


}
