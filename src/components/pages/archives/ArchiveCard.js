import React from "react";

import DataSets from "./DataSets";

// display a single archive on a card
export default function ArchiveCard({ archive }) {
  return (
      <div className="grid__cell card">
          <div className="card__content card__content--background">
              <h2 className="card__title">{archive.name}</h2>

              <div className="grid__cell card card--no-animation">
                  <div className="card__content card__content--background card--no-animation">
                    <table>
                        <tr>
                            <td className="custom-vertical-align-top">
                                <img className="custom-archive-img" src={archive.thumbnail} alt=""/></td>
                            <td><DataSets archive={archive} /></td>
                        </tr>
                    </table>
                      <hr></hr>
                      <h3 className="card__title card--no-animation">{archive.short_description}</h3>
                      <p className="card__title card--no-animation">{archive.long_description}</p>
                  </div>
              </div>

              <div className="grid__cell card">
                  <div className="card__content card__content--background">
                      <h3>Data Retrieval</h3>
                      <p className="card__title">{archive.retrieval_description}</p>
                  </div>
              </div>


          </div>
    </div>
  );
}
