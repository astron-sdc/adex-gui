import {useContext, useEffect} from "react";
import {UIContext} from "../../../contexts/UIContext";
import {
    ArchiveSelection,
    CalibrationLevel,
    Collection, ColorMap,
    DataProductType,
    Dec,
    FoV,
    FrequencySlider,
    Ra,
    SkyViewHeader,
    Survey
} from "../../StaticConfig";
import {
    getGroupedSetterFunction,
    getGroupedSetterWithRangeObjectDependencyFunction,
    getGroupedSetterWithSetObjectDependencyFunction,
    setGroupedSetterValue,
} from "../../../utils/Util";
import {QueryContext, QueryStatuses} from "../../../contexts/QueryContext";

export function getAllUniqueFields(object, objectField) {
    const uniqueFields = [...new Set(object.map(item => Object.values(item)[0][objectField]).flat())];
    if (uniqueFields.includes(undefined)) {
        return []
    }
    return Object.values(uniqueFields);
}

export function SkyViewController() {
    const {
        skyView, setSkyView
    } = useContext(UIContext);

    const {status: queryStatus, runQuery} = useContext(QueryContext);

    //when the datasets object changes also the dependent filters change like archive and level
    useEffect(() => {
        if (!skyView.status) {
            return;
        }
        setGroupedSetterValue(setSkyView, 'selectedLevels', getAllUniqueFields(skyView.selectedDatasets, 'level'));
        setGroupedSetterValue(setSkyView, 'selectedArchives', getAllUniqueFields(skyView.selectedDatasets, 'archive'));
        setGroupedSetterValue(setSkyView, 'selectedCollections', getAllUniqueFields(skyView.selectedDatasets, 'collection'));
        setGroupedSetterValue(setSkyView, 'selectedDataproductTypes', getAllUniqueFields(skyView.selectedDatasets, 'dataproduct_type'));
    }, [skyView.selectedDatasets]);

    // when the filters change also the dependent frequency fields have to change
    useEffect(() => {
        if (!skyView.status) {
            return;
        }

        //if the filters changed manually or because loaded configuration, the frequencies should NOT be updated.
        // otherwise, the frequencies are already set to min/max values; we don't want a circular loop
        if (skyView.status === "config_loaded" || skyView.status === "program change") {
            const minimumFrequency = Math.min(...getAllUniqueFields(skyView.selectedDatasets, "freq_min"));
            const maximumFrequency = Math.max(...getAllUniqueFields(skyView.selectedDatasets, "freq_max"));
            setGroupedSetterValue(setSkyView, 'frequencyMin', minimumFrequency);
            setGroupedSetterValue(setSkyView, 'frequencyMax', maximumFrequency);
        }
        setGroupedSetterValue(setSkyView, 'status', "program change");

    }, [skyView.selectedArchives, skyView.selectedCollections, skyView.selectedLevels, skyView.selectedDataproductTypes]);

    if (!skyView.status) {
        return;
    }

    SkyViewHeader.bindButtonClick(() => runQuery(skyView?.selectedDatasets, skyView));
    SkyViewHeader.isButtonActive = queryStatus !== QueryStatuses.loading && skyView.selectedDatasets.length > 0;

    const surveyNames = skyView.surveys.map(survey => survey.name);
    Survey.bindDropdown(getGroupedSetterFunction(setSkyView, 'selectedSurvey'), surveyNames, skyView.selectedSurvey);
    ColorMap.bindDropdown(getGroupedSetterFunction(setSkyView, 'selectedColorMap'), skyView.colorMaps, skyView.selectedColorMap);
    Ra.bindInputField(getGroupedSetterFunction(setSkyView, 'ra'), skyView.ra);
    Dec.bindInputField(getGroupedSetterFunction(setSkyView, 'dec'), skyView.dec);
    FoV.bindInputField(getGroupedSetterFunction(setSkyView, 'fov'), skyView.fov);

    //below elements are dependent on the selected datasets
    FrequencySlider.bindRangeSlider(
        getGroupedSetterWithRangeObjectDependencyFunction(setSkyView, "frequencyMin", skyView.datasets, 'selectedDatasets', 'freq_max', true),
        getGroupedSetterWithRangeObjectDependencyFunction(setSkyView, "frequencyMax", skyView.datasets, 'selectedDatasets', 'freq_min'),
        skyView.frequencyMin, skyView.frequencyMax);
    DataProductType.bindCheckbox(
        getGroupedSetterWithSetObjectDependencyFunction(setSkyView, 'selectedDataproductTypes', skyView.datasets, skyView.selectedDatasets, 'selectedDatasets', skyView.selectedDataproductTypes, 'dataproduct_type'),
        skyView.dataproductTypes, skyView.selectedDataproductTypes);
    CalibrationLevel.bindCheckbox(
        getGroupedSetterWithSetObjectDependencyFunction(setSkyView, 'selectedLevels', skyView.datasets, skyView.selectedDatasets, 'selectedDatasets', skyView.selectedLevels, 'level'),
        skyView.levels, skyView.selectedLevels);
    ArchiveSelection.bindCheckbox(
        getGroupedSetterWithSetObjectDependencyFunction(setSkyView, 'selectedArchives', skyView.datasets, skyView.selectedDatasets, 'selectedDatasets', skyView.selectedArchives, 'archive'),
        skyView.archives, skyView.selectedArchives);
    Collection.bindCheckbox(
        getGroupedSetterWithSetObjectDependencyFunction(setSkyView, 'selectedCollections', skyView.datasets, skyView.selectedDatasets, 'selectedDatasets', skyView.selectedCollections, 'collection'),
        skyView.collections, skyView.selectedCollections);
}
