import React, {useContext} from "react";
import AladinSkyView from './AladinSkyView'
import QueryResultsTable from "../../../views/page/QueryResultsTable";
import {QueryContext} from "../../../contexts/QueryContext";


export default function QueryViewPage() {
    const {
        data,
        status
    } = useContext(QueryContext)

    return <div>
        <AladinSkyView key="aladin-skyview" data={data}/>
        <QueryResultsTable key="skyview-query-results-table"
                              name="skyview"
                              status={status}
                              data={data}
                              totalDataSize={data.length}
                              idleMessage={"Click the button 'Query' to retrieve the data and show the results"}
                              size={"small"}
                              hasGoTo={true}/>
    </div>
}