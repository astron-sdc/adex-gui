import datasets from "../../../datasets.json";
import expect from "expect";
import {getAllUniqueFields} from "./SkyViewController";
import axios from "axios";

describe("Should get all available unique fields from an object", () => {
    it("on string field", () => {
        const archives = getAllUniqueFields(datasets, 'archive')
        expect(archives).toStrictEqual(["lofar", "astron_vo", "apertif"]);
    });
    it("on list field", () => {
        const archives = getAllUniqueFields(datasets, 'dataproduct_type')
        expect(archives).toStrictEqual(["visibility", "cube", "image", "timeSeries"]);
    });
    it("on number field", () => {
        const archives = getAllUniqueFields(datasets, 'freq_min')
        expect(archives).toStrictEqual([10, 1338, 1410, 30, 147, 120]);
    });
    it("on unknown field", () => {
        const archives = getAllUniqueFields(datasets, 'unknown')
        expect(archives).toStrictEqual([]);
    });
});

jest.setTimeout(100000);
it("check config state change; if the esap adex.py configuration is changed, it is detected here." +
        "Copy paste the datasets object to the json file to see if filtering still works. " +
        "If not, fix usages of dependencies on the datasets. " +
        "For example the function 'getGroupedSetterWithDatasetsDependencyFunction'", async () => {
    await axios
            .get("https://sdc-dev.astron.nl:5557/esap-api/query/configuration?name=adex")
            .then((response) => {
                expect(response.data["configuration"].settings.datasets).toStrictEqual(datasets);
            })
    })