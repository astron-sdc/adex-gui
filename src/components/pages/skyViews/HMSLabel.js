import React from "react";
import { toHMSLabel, toFOVLabel } from '../../../utils/coordinates'

export default function HMSLabel(props) {
    let ra_label =  Number((parseFloat(props.ra)).toFixed(2))
    let dec_label =  Number((parseFloat(props.dec)).toFixed(2))
    let radec_label = ra_label + ', ' + dec_label

    return <div>{toHMSLabel(radec_label)} {toFOVLabel(props.fov)}</div>
}