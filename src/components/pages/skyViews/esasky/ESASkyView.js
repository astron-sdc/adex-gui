import React, {useContext} from "react";
import {Card, Button, Table} from 'react-bootstrap'
import {SkyViewContext} from "../../../../contexts/SkyViewContext";
import ESASkyFrame from './ESASkyFrame'
import {UIContext} from "../../../../contexts/UIContext";
import {setGroupedSetterValue} from "../../../../utils/Util";

// how to do this stuff?
// https://stackoverflow.com/questions/47635318/handle-iframe-in-react-with-refs
// https://stackoverflow.com/questions/70623283/what-type-to-use-in-useref-of-iframe
export default function ESASkyView(props) {
    const {
        refreshESASky, setRefreshESASky,
        sendMessageToESASky
    } = useContext(SkyViewContext);
    const {
        skyView,
        setSkyView
    } = useContext(UIContext);

    const gotoRADec = (ra, dec) => {
        setGroupedSetterValue(setSkyView, 'ra', ra);
        setGroupedSetterValue(setSkyView, 'dec', dec);
        // reload the current query
        setGroupedSetterValue(setSkyView, 'refreshQuery', !skyView.refreshQuery);
    }


    const sendMessage = (message) => {
        sendMessageToESASky(message)

        // reload the current query
        //setRefreshQuery(!refreshQuery)
    }

    const doRefreshESASky = () => {
        setRefreshESASky(!refreshESASky)
    }

    const doRefreshQuery = () => {
        setGroupedSetterValue(setSkyView, 'refreshQuery', !skyView.refreshQuery);
    }

    return (
        <Card>
            <Card.Body>
                <Table>
                    <div>
                        <ESASkyFrame data={props.data}/>
                    </div>
                </Table>
                <Button variant="primary" onClick={() => doRefreshQuery()}>Refresh Query</Button>&nbsp;
                <Button variant="primary" onClick={() => doRefreshESASky()}>Refresh ESASky</Button>&nbsp;
                <Button variant="primary" onClick={() => gotoRADec(340.0, 34.0)}>Goto 340,34</Button>&nbsp;
                <Button variant="primary" onClick={() => sendMessage({
                    event: 'getCenter',
                    content: {cooFrame: 'J2000'}
                })}>getCenter</Button>
            </Card.Body>

        </Card>

    );

}


