import React, {useRef, useEffect, useContext} from "react";
import {SkyViewContext} from "../../../../contexts/SkyViewContext";
import {UIContext} from "../../../../contexts/UIContext";

export const ESASKY_URL = "https://sky.esa.int?hide_welcome=true&sci=true"
//export const ESASKY_URL = "http://sky.esa.int?hide_welcome=true&sci=true&layout=clean"

export default function ESASkyFrame(props) {
    const {message, refreshESASky} = useContext(SkyViewContext);
    const {
        skyViewRa,
        skyViewDec,
        skyViewFov,
        skyViewCollection,
        survey,
    } = useContext(UIContext);
    const myRef = useRef(null)

    // this is the listener to capture messages from ESASky
    window.addEventListener("message", function (e) {
        console.log(e)
        if (e.data.values) {
            if ((e.data.values.ra) && (e.data.values.dec)) {
                alert(e.data.values.ra + ',' + e.data.values.dec)
            }
        }
        // Code to handle the data
    });

    useEffect(() => {
        // initialisation of ESASky
        //alert('init')
        myRef.current.contentWindow.postMessage({
                event: 'setModuleVisibility', content: {
                    modules:
                        {
                            feedback_button: false,
                            jwst_planning_button: false,
                            screenshot_button: false,
                            scientific_toggle_button: false,
                            language_button: false,
                            dice_button: false,
                            share_button: false
                        }
                }
            },
            ESASKY_URL);
    }, [refreshESASky]);

    useEffect(() => {
        // used to send messages to ESASky
        // example: sendMessageToESASky({event:'goToTargetName', content: {targetName:'M51'}})

        // the message string is stored in the SkyViewContext
        // and then triggered here by the change of the 'refreshMessage' toggle

        if (message.event) {
            // only sent a message if there is something to send
            //alert("useEffect (refreshESASky) => " + message.event + ": "+message.content)
            myRef.current.contentWindow.postMessage(message, ESASKY_URL);
        }

        myRef.current.contentWindow.postMessage({
                event: 'setModuleVisibility', content: {
                    modules:
                        {
                            feedback_button: false,
                            jwst_planning_button: false,
                            screenshot_button: false,
                            scientific_toggle_button: false,
                            language_button: false,
                            dice_button: false,
                            share_button: false
                        }
                }
            },
            ESASKY_URL);
    }, [refreshESASky]);


    useEffect(() => {
        // refresh the view when RA,Dec or FoV are changed
        //alert('useEffect (skyCoords, fov) => goToRaDec, setFov')
        myRef.current.contentWindow.postMessage({
            event: 'goToRaDec',
            content: {ra: skyViewRa, dec: skyViewDec}
        }, ESASKY_URL);

        if (skyViewFov > 0) {
            myRef.current.contentWindow.postMessage({event: 'setFov', content: {fov: skyViewFov}}, ESASKY_URL);
        }
    }, [skyViewDec, skyViewDec, skyViewFov]);


    useEffect(() => {
        // change the background HiPS
        //alert('useEffect (survey) => changeHipsWithParams')
        myRef.current.contentWindow.postMessage({
            event: 'addHiPSWithParams',
            content: {
                'hips': {
                    'name': 'lotss_dr2_high',
                    'id': 'lotss_dr2_high',
                    'url': 'https://hips.astron.nl/ASTRON/P/lotss_dr2_high',
                    'cooframe': 'J2000',
                    'maxnorder': '11',
                    'imgformat': 'png'
                }
            }
        }, ESASKY_URL);

        myRef.current.contentWindow.postMessage({
            event: 'changeHipsWithParams',
            content: {
                'hips': {
                    'name': 'lotss_dr2_high',
                    'id': 'lotss_dr2_high',
                    'url': 'https://hips.astron.nl/ASTRON/P/lotss_dr2_high',
                    'cooframe': 'J2000',
                    'maxnorder': '11',
                    'imgformat': 'png'
                }
            }
        }, ESASKY_URL);

    }, [survey]);


    useEffect(() => {
        // refresh the data
        //alert("useEffect (fetchedData, refreshESASky) => overlayCatalogue, goToRaDec, setFov")

        // do we want an ESASky view with a results table? or not?
        let withDetails = true
        let content = constructESASkyCatalogue(skyViewCollection, props.data, withDetails)

        // clear the overlay
        //myRef.current.contentWindow.postMessage({'event': 'deleteCatalogue', content: {'overlayName': collection}}, ESASKY_URL);

        // create the overlay
        if (withDetails) {
            myRef.current.contentWindow.postMessage({
                event: 'overlayCatalogueWithDetails',
                content: content
            }, ESASKY_URL);
        } else {
            myRef.current.contentWindow.postMessage({
                event: 'overlayCatalogue',
                content: content
            }, ESASKY_URL);
        }

        // move to the current position and fov
        myRef.current.contentWindow.postMessage({
            event: 'goToRaDec',
            content: {ra: skyViewRa, dec: skyViewDec}
        }, ESASKY_URL);
        if (skyViewFov > 0) {
            myRef.current.contentWindow.postMessage({event: 'setFov', content: {fov: skyViewFov}}, ESASKY_URL);
        }
    }, [refreshESASky]);


    function constructESASkyCatalogue(collection, data, withDetails) {
        //{'event': 'overlayCatalogue', content: {'overlaySet': {'type': 'SourceListOverlay', 'overlayName': 'test catalogue name', 'cooframe': 'J2000', 'color': '#ee2345', 'lineWidth': 10, 'skyObjectList': [{'name': 'source name A', 'id': 1, 'ra': '150.44963', 'dec': '2.24640'}]}}}
        //let content = {'overlaySet': {'type': 'SourceListOverlay', 'overlayName': 'test catalogue name', 'cooframe': 'J2000', 'color': '#ee2345', 'lineWidth': 10, 'skyObjectList': [{'name': 'source name A', 'id': 1, 'ra': '150.44963', 'dec': '2.24640'},{'name': 'source name A', 'id': 1, 'ra': '150.54963', 'dec': '2.24640'}]}}

        let skyObjectList = []
        if (data) {
            data.forEach(function (object) {
                console.log(object)
                //alert(object)
                let skyObject = {
                    'name': object.name,
                    'id': 1,
                    'ra': object.ra.toString(),
                    'dec': object.dec.toString()
                }

                if (withDetails) {
                    let data = [
                        {'name': 'collection', 'value': object.collection, 'type': 'STRING'},
                        {'name': 'dataset', 'value': object.dataset, 'type': 'STRING'},
                        {'name': 'level', 'value': object.level, 'type': 'INTEGER'},
                        {'name': 'url', 'value': object.url, 'type': 'STRING'},
                        {'name': 'thumbnail', 'value': object.thumbnail, 'type': 'URL'},
                    ]
                    skyObject.data = data
                }
                //alert(skyObject.data[0].url)
                skyObjectList.push(skyObject)

            })
        }
        let content = {
            'overlaySet': {
                'type': 'SourceListOverlay',
                'overlayName': collection,
                'cooframe': 'J2000',
                'color': '#ee2345',
                'lineWidth': 10,
                'skyObjectList': skyObjectList
            }
        }

        return content
    }


    return (
        <div>
            <iframe ref={myRef} name='myRef' id='myRef' src={ESASKY_URL} width="100%" height={750}/>
        </div>
    )
}