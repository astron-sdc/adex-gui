import React, {useContext, useEffect} from "react";

import {SkyViewContext} from "../../../contexts/SkyViewContext";
import {UIContext} from "../../../contexts/UIContext";
import LoadingSpinner from "../../LoadingSpinner";
import Spinner from "../../../views/basics/Spinner";

const AladinSkyView = (props) => {
    const {
        skyView,
    } = useContext(UIContext);
    const {
        setNewSkyCoords, setNewFov, selectedDataItem, setSkyObject
    } = useContext(SkyViewContext);

    const {data} = props;

    useEffect(() => {
        const drawAladin = () => {
            if (!skyView.selectedSurvey || !skyView.selectedColorMap) {
                return <Spinner message={"Loading Aladin"} size={"h3"}/>
            }
            const aladinWindowConfig = {
                showFrame: false,
                showLayersControl: false,
                showGotoControl: false,
                showZoomControl: false,
                reticleColor: '#00adee',
                reticleSize: 30
            };
            let aladin = window.A.aladin('#aladin-lite-div', aladinWindowConfig)
            aladin.setImageSurvey(skyView.selectedSurvey)

            aladin.getBaseImageLayer().getColorMap().update(skyView.selectedColorMap);

            aladin.gotoRaDec(skyView.ra, skyView.dec)

            aladin.setFov(parseFloat(skyView.fov))


            if (skyView.surveys) {
                skyView.surveys.forEach((survey_config) => {
                    // only create survey layers for hips files
                    if (survey_config['hips_name']) {
                        aladin.createImageSurvey(
                            survey_config['hips_name'],
                            survey_config['hips_name'],
                            survey_config['hips_url'],
                            "equatorial",
                            13,
                            {imgFormat: survey_config['format']}
                        )
                    }
                });
            }

            //aladin.getBaseImageLayer().getColorMap().update('cubehelix');
            // possible values are 'native', 'grayscale', 'cubehelix', 'eosb' and 'rainbow'
            //aladin.getBaseImageLayer().getColorMap().update('cubehelix');

            // create the catalog layer
            if (data && data.length !== 0) {
                createLayers(aladin, data)
            }

            // add a listener to aladin
            // see aladin source code:
            // Aladin.AVAILABLE_CALLBACKS = ['select', 'objectClicked', 'objectHovered', 'footprintClicked', 'footprintHovered', 'positionChanged', 'zoomChanged', 'click', 'mouseMove', 'fullScreenToggled'];

            // callback when the sky position in aladin is changed
            aladin.on('positionChanged', function () {
                let radec = aladin.getRaDec()
                setNewSkyCoords(radec)
            })

            // callback when the field of view in aladin is changed
            aladin.on('zoomChanged', function () {
                let fov = aladin.getFov()
                setNewFov(fov)
            })


            aladin.on('objectClicked', function (object) {

                // when an object is hovered, store the ra,dec,fov in the global state
                // (because I found no better or more accurate way of doing this).

                if (object) {

                    try {
                        // select the object under the mouse cursor, and store it in global state
                        //setSelectedDataItem(object.data.name)
                        setSkyObject(object.data)
                    } catch (e) {
                    }
                }
            });

        }
        drawAladin()
    }, [selectedDataItem, setSkyObject, data, skyView.selectedSurvey, skyView.selectedColorMap])


    const addCirclesToOverlay = (my_overlay, object, color, size) => {
        my_overlay.add(window.A.circle(object.ra, object.dec, size, {color: color, lineWidth: 2}));
    }

    // create the catalog layer
    const createLayers = (aladin, data) => {
        aladin.removeLayers()

        let overlay_selected = window.A.graphicOverlay({name: 'selected', color: 'red', lineWidth: 3});
        aladin.addOverlay(overlay_selected);

        //let overlay_dataset_1 = window.A.graphicOverlay({name: 'astron_vo-apertif-dr1',color: 'green', lineWidth: 5});
        //aladin.addOverlay(overlay_dataset_1);

        let my_catalog = window.A.catalog({
            name: skyView.collection,
            shape: 'square',
            color: '#00529b',
            sourceSize: 15,
            //labelColumn: 'name',
            displayLabel: true,
            onClick: 'showPopup'
        });
        //onClick: 'showTable'});

        // loop through all the objects and add them to the appropriate layer based a property
        if (data) {

            data.forEach(function (object) {

                if (selectedDataItem) {

                    if (object.name === selectedDataItem) {
                        addCirclesToOverlay(overlay_selected, object, "#00adee", 0.3)
                    }
                }
                // draw an extra overlay
                //if (object.dataset=='astron_vo-apertif-dr1') {
                //   addCirclesToOverlay(overlay_dataset_1, object, "green",0.2)
                //}

                // draw a clickable icon for each observation
                addToCatalog(my_catalog, object)
            })

            aladin.addCatalog(my_catalog);

            // add Simbad catalog
            //aladin.addCatalog(window.A.catalogFromSimbad('m45', 0.2, {shape: 'plus', color : '#5d5', onClick: 'showTable'}));

        }
    }

    const addToCatalog = (my_catalog, object) => {
        let source = [window.A.source(
            // if ra < 0 then add 360 to make it a positive number
            object.ra,
            object.dec,

            {
                name: object.name,
                collection: object.collection,
                type: object.dataproduct_type,
                level: object.calibration_level,
                download: '<a href="' + object.url + '" target="_blank">download</a>',
                thumbnail: '<a href="' + object.thumbnail + '" target="_blank">thumbnail</a>',
            },
        )]

        my_catalog.addSources(source);
    }

    return (
        <div>
            <div id='aladin-lite-div' className="aladin"/>
        </div>
    )
}

export default AladinSkyView