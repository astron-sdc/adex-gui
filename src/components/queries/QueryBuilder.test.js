import {useState} from "react";
import {act, renderHook} from "@testing-library/react-hooks";
import createParamObjectMapping from "./QueryBuilder";
import expect from "expect";
import {setGroupedSetterValueWithSetObjectDependency} from "../../utils/Util";

describe("create parameter object mapping", () => {
    const datasets = [
        {
            "x-men-wolverine": {
                "uri": "x-men",
                "collection": "wolverine",
                "impact_min": 37,
                "impact_max": 73,
                "dataproduct_type": "claws",
            },
        },
        {
            "x-men-hulk": {
                "uri": "x-men",
                "collection": "hulk",
                "impact_min": 9,
                "impact_max": 90,
                "dataproduct_type": "fists",
            },
        },
        {
            "batman-robin": {
                "uri": "batman",
                "collection": "robin",
                "impact_min": 83,
                "impact_max": 91,
                "dataproduct_type": "fists",
            },
        }
    ]

    function useSuperHeroState() {
        const [superHeroes, setSuperHeroes] = useState({
            weapons: ["claws", "fists"],
            ra: 1,
            dec: 2,
            fov: 3,
            selectedDatasets: datasets,
            pageSize: 10
        });
        return {superHeroes, setSuperHeroes};
    }

    it("Get params per selected dataset before altering group", () => {
        const {result} = renderHook(() => useSuperHeroState());
        let params = createParamObjectMapping(result.current.superHeroes.selectedDatasets[0], result.current.superHeroes)
        expect(params).toStrictEqual({
            "dataset_uri": "x-men",
            "collection": "wolverine",
            "dataproduct_type": "claws",
            "ra": 1,
            "dec": 2,
            "fov": 3,
            "page_size": 10
        })
        params = createParamObjectMapping(result.current.superHeroes.selectedDatasets[1], result.current.superHeroes)
        expect(params).toStrictEqual({
            "dataset_uri": "x-men",
            "collection": "hulk",
            "dataproduct_type": "fists",
            "ra": 1,
            "dec": 2,
            "fov": 3,
            "page_size": 10
        })
        params = createParamObjectMapping(result.current.superHeroes.selectedDatasets[2], result.current.superHeroes)
        expect(params).toStrictEqual({
            "dataset_uri": "batman",
            "collection": "robin",
            "dataproduct_type": "fists",
            "ra": 1,
            "dec": 2,
            "fov": 3,
            "page_size": 10
        })
    });
    it("Get params for the selected dataset after altering group", () => {
        const {result} = renderHook(() => useSuperHeroState());
        act(() => {
            setGroupedSetterValueWithSetObjectDependency(result.current.setSuperHeroes, "weapons", ["fists"], datasets, result.current.superHeroes.selectedDatasets, 'selectedDatasets', result.current.superHeroes.weapons);
        });
        expect(result.current.superHeroes.selectedDatasets).toStrictEqual([datasets[1], datasets[2]])
        let params = createParamObjectMapping(result.current.superHeroes.selectedDatasets[0], result.current.superHeroes)
        expect(params).toStrictEqual({
            "dataset_uri": "x-men",
            "collection": "hulk",
            "dataproduct_type": "fists",
            "ra": 1,
            "dec": 2,
            "fov": 3,
            "page_size": 10
        })
        params = createParamObjectMapping(result.current.superHeroes.selectedDatasets[1], result.current.superHeroes)
        expect(params).toStrictEqual({
            "dataset_uri": "batman",
            "collection": "robin",
            "dataproduct_type": "fists",
            "ra": 1,
            "dec": 2,
            "fov": 3,
            "page_size": 10
        })
    });
})