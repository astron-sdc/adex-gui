import {isEmptyObject} from "../../utils/Util";
import axios from "axios";

const mapping = {
    //dataset fields
    "uri": "dataset_uri",
    "level": "level",
    "collection": "collection",
    "dataproduct_type": "dataproduct_type",
    //group fields
    "ra": "ra",
    "dec": "dec",
    "fov": "fov",
    "pageSize": "page_size"
}

function mapKeysWithValue(objectEntries, params) {
    objectEntries.forEach(([key, value]) => {
        const mappingKey = mapping[key];
        if (mappingKey === undefined) { //No mapping present means ignore parameter
            return;
        }
        params[mappingKey] = value;
    })
}

/**
 * This function maps the values where the selected object's key is mapped to a key that is accepted as a parameter by the endpoint
 **/
export default function createParamObjectMapping(selectedObject, groupObject) {
    let params = {}
    mapKeysWithValue(Object.entries(Object.values(selectedObject)[0]), params);
    mapKeysWithValue(Object.entries(groupObject), params);
    return params
}

function shouldIgnoreValue(value) {
    return isEmptyObject(value)
        || value === ""
        || value.length === 0;
}

function constructUrl(apihost, path, parameters) {
    let isFirstParam = true;
    let allParamsUrlFormat = "?";
    Object.entries(parameters).forEach(([key, value]) => {
        if (!shouldIgnoreValue(value)) {
            let param = key + "=" + value;
            if (isFirstParam) {
                allParamsUrlFormat = allParamsUrlFormat + param;
                isFirstParam = false;
            } else {
                allParamsUrlFormat = allParamsUrlFormat + "&" + param;
            }
        }
    });
    return apihost + path + allParamsUrlFormat;
}

function fetchData(apihost, path, parameters) {
    const url = constructUrl(apihost, path, parameters);
    return axios.get(url)
        .then((response) => response.data)
        .catch((error) => {
            console.error("Query with url '" + url + "' resulted in:\n" + error.toString());
        });
}

export async function fetchGroup(api_host, queryPath, groupTarget, group) {
    return Promise.all(groupTarget?.map(async dataset => {
        const params = createParamObjectMapping(dataset, group)
        return fetchData(api_host, queryPath, params);
    }));
}