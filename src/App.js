import React from "react";
import "./App.css";
import packageJson from '../package.json';

import {GlobalContextProvider} from "./contexts/GlobalContext";
import {BasketContextProvider} from "./contexts/BasketContext";
import {SkyViewContextProvider} from "./contexts/SkyViewContext";
import {UIContextProvider} from "./contexts/UIContext";
import Routes from "./components/Routes";
import {QueryContextProvider} from "./contexts/QueryContext";

// This is the App, only application global stuff goes here, like the global state provider.
export default function App() {
    return (
        <div>
            <GlobalContextProvider>
                <BasketContextProvider>
                        <QueryContextProvider>
                            <UIContextProvider>
                                <SkyViewContextProvider>
                                    <Routes/>
                                </SkyViewContextProvider>
                            </UIContextProvider>
                        </QueryContextProvider>
                </BasketContextProvider>
            </GlobalContextProvider>
            <footer><small>version {packageJson.version}</small></footer>
        </div>
    );
}
