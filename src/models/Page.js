export class Page {

    constructor(name, icon, content, menu) {
        this.name = name;
        this.icon = icon;
        this.content = content;
        this.menu = menu;
    }

    static getRoute(page) {
        return "/" + page.name.toLowerCase();
    }
}
