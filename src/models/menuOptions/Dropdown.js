export class Dropdown {

    constructor(title, tooltipInfo = undefined, titleClass = undefined, tooltipPosition = undefined) {
        this.title = title;
        this.titleClass = titleClass;
        this.tooltipInfo = tooltipInfo;
        this.tooltipPosition = tooltipPosition;
        this.values = [];
        this.currentValue = undefined;
        this.handleChange = undefined;
    }

    bindDropdown(setter, allValues, currentValue) {
        this.handleChange = setter;
        this.values = allValues;
        this.currentValue = currentValue;
    }

}