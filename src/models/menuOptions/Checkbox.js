export class Checkbox {

    constructor(title, tooltipInfo = undefined) {
        this.title = title;
        this.tooltipInfo = tooltipInfo
        this.values = []; //It can either be an array or a dictionary of the value with tooltip: {"value" : {"tooltip"}
        this.checkedValues = [];
        this.handleChange = undefined;
    }

    bindCheckbox(setter, values, checkedValues) {
        this.handleChange = setter;
        this.values = values;
        this.checkedValues = checkedValues;
    }
}
