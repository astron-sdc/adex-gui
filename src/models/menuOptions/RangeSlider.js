export class RangeSlider {

    constructor(title, tooltipInfo = undefined, min = 0, max = 1500, step = 10) {
        this.title = title;
        this.tooltipInfo = tooltipInfo;
        this.min = min;
        this.max = max;
        this.currentMin = min;
        this.currentMax = max;
        this.step = step;
        this.handleChangeMin = undefined;
        this.handleChangeMax = undefined;
    }

    bindRangeSlider(setterMin, setterMax, currentMin, currentMax) {
        this.handleChangeMin = setterMin;
        this.handleChangeMax = setterMax;
        this.currentMin = currentMin;
        this.currentMax = currentMax;
    }
}