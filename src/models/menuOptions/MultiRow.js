export class MultiRow {
    constructor(title, tooltipInfo = undefined, menuOptions) {
        this.title = title;
        this.tooltipInfo = tooltipInfo
        this.menuOptions = menuOptions;
    }
}