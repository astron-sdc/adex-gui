export class Header {

    constructor(title, tooltipInfo = undefined, buttonTitle, buttonInfo, icon) {
        this.title = title;
        this.tooltipInfo = tooltipInfo;
        this.buttonTitle = buttonTitle;
        this.buttonInfo = buttonInfo;
        this.isButtonActive = false;
        this.iconName = icon
        this.handleClick = undefined;
    }

    bindButtonClick(handleClickFunction) {
        this.handleClick = handleClickFunction;
        this.isButtonActive = true;
    }
}
