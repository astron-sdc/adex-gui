export class InputField {

    constructor(title, tooltipInfo = undefined, typeStylingTuple) {
        this.title = title;
        this.tooltipInfo = tooltipInfo
        this.typeStylingTuple = typeStylingTuple;
        this.handleChange = undefined;
        this.currentValue = undefined;
    }

    bindInputField(setter, value) {
        this.handleChange = setter;
        this.currentValue = value;
    }
}