/**
 * This menu class contains all the elements in the 'options' folder where the specific menu elements are (i.e header, dropdown, checkboxes etc.).
 * Every specific menu element is grouped here.
 *
 * To render the menu as a component, in the folder 'views' there is a MenuContent class
 * in that class, menu elements constructed here are called in the same order as the constructor depicts
 *
 * Example: create a menu called 'Example' with a header and elements (dropdown + checkbox)
 *
 * Models: see folder 'models/menu'
 * 1] in class Header.js define: static ExampleHeader = new Header("Example", "buttonTitleExample", "buttonInfoExample", "thumbs-up"
 * 2] in class Dropdown.js define: static Example = [new Dropdown([options]), new Checkbox([options])
 * 3] in class Menu.js, define: static Example = new Menu(ExampleHeader, ExampleElements, undefined)
 *
 * View:  see folder 'views/menu'
 * 1] call function with menu: MenuContent(Menu.Example)
 */
export class Menu {
    constructor(header, elements, advancedOptions) {
        this.header = header
        this.elements = elements;
        this.advancedOptions = advancedOptions;
    }
}
