import React, {createContext, useContext, useEffect, useState} from "react";
import axios from "axios";
import {GlobalContext} from "./GlobalContext";

export const UIContext = createContext();

export function UIContextProvider({children}) {
    const {api_host} = useContext(GlobalContext);

    const [archives, setArchives] = useState({
        allArchives: undefined,
        archiveShortDescriptions: undefined,
        selectedArchives: []
    });

    const [skyView, setSkyView] = useState({
        surveys: [],
        colorMaps: [],
        datasets: [],
        selectedDatasets: [],
        selectedSurvey: "P/DSS2/color",
        selectedColorMap: undefined,
        archives: [],
        selectedArchives: [],
        collections: [],
        selectedCollections: [],
        levels: [],
        selectedLevels: [],
        dataproductTypes: [],
        selectedDataproductTypes: [],
        ra: 340,
        dec: 34,
        fov: 15,
        frequencyMin: undefined,
        frequencyMax: undefined,
        refreshFactor: undefined,
        pageSize: undefined,
        status: undefined,
    });

    useEffect(() => {
        axios
            .get(api_host + "query/configuration?name=adex")
            .then((response) => {
                let config = response.data["configuration"];
                if (config.settings) {
                    let defaults = config.settings.defaults;
                    if (defaults) {
                        setSkyView(prev => ({
                            ...prev,
                            surveys: config.settings.surveys,
                            colorMaps: config.settings.color_maps,
                            datasets: config.settings.datasets,
                            selectedDatasets: config.settings.datasets,
                            selectedSurvey: defaults.survey,
                            selectedColorMap: defaults.color_map,
                            archives: defaults.archives,
                            selectedArchives: defaults.archives,
                            collections: defaults.collections,
                            selectedCollections: defaults.collections,
                            levels: defaults.calibration_levels,
                            selectedLevels: defaults.calibration_levels,
                            dataproductTypes: defaults.dataproduct_types,
                            selectedDataproductTypes: defaults.dataproduct_types,
                            ra: defaults.ra,
                            dec: defaults.dec,
                            fov: defaults.fov,
                            frequencyMin: defaults.f_min,
                            frequencyMax: defaults.f_max,
                            refreshFactor: defaults.refreshFactor,
                            pageSize: defaults.pageSize,
                            status: "config_loaded",
                        }));
                    }
                }


            })
    }, [api_host]);

    useEffect(() => {
        axios
            .get(api_host + "query/archives-uri")
            .then((response) => {
                let archivesInformation = response.data.results;
                const archivesDict = {};

                archivesInformation.forEach(archiveInfo => {
                    archivesDict[archiveInfo.name] = archiveInfo.short_description;
                });

                setArchives((prev => ({
                    ...prev,
                    archiveShortDescriptions: archivesDict,
                    allArchives: archivesInformation,
                    selectedArchives: Object.keys(archivesDict),
                })))
            })
    }, [api_host]);

    return (
        <UIContext.Provider
            value={{
                archives,
                setArchives,
                skyView,
                setSkyView,
            }}
        >
            {children}
        </UIContext.Provider>
    )
}
