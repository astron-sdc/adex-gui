import React, {createContext, useContext, useState} from "react";

import {GlobalContext} from "./GlobalContext";
import {fetchGroup} from "../components/queries/QueryBuilder";

export function analyzeAndCombineData(queryResults) {
    let combinedData = []
    try {
        queryResults?.forEach(result => {
            const data = result?.results;
            if (data === undefined) {
                return;
            }
            if (data instanceof Array) {
                combinedData = combinedData.concat(data);
            } else {
                let isValid = !data.toLowerCase().includes('error');
                if (isValid) {
                    combinedData.push(data);
                }
            }
        })
    } catch (error) {
        combinedData = "Error combining data from the query results:\n" + error.message;
    }
    return combinedData;
}

export const QueryContext = createContext();

export const QueryStatuses = {
  idle: 'idle',
  loading: 'loading',
  success: 'success',
  error: 'error'
}

export function QueryContextProvider({children}) {
    const {api_host} = useContext(GlobalContext);
    const queryPath = "query/query/";

    const [data, setData] = useState([]);
    const [status, setStatus] = useState(QueryStatuses.idle);

    async function runQuery(groupTarget, group) {
        setStatus(QueryStatuses.loading)
        const results = await fetchGroup(api_host, queryPath, groupTarget, group);
        let combinedData = analyzeAndCombineData(results);
        setData(combinedData);
        if (combinedData instanceof Array) {
            setStatus(QueryStatuses.success);
        } else {
            setStatus(QueryStatuses.error)
        }
    }

    return (
        <QueryContext.Provider
            value={{
                data,
                status,
                runQuery,
            }}
        >
            {children}
        </QueryContext.Provider>
    )
}