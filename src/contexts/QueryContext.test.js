import React, {useContext} from "react";
import expect from "expect";
import renderer from 'react-test-renderer';
import {QueryContext, QueryContextProvider, QueryStatuses} from "./QueryContext";
import {GlobalContext} from "./GlobalContext";
import Button from "../views/basics/Button";
import {act} from "@testing-library/react-hooks";
import axios from "axios";
import {waitFor} from '@testing-library/react';

const nock = require('nock')
axios.defaults.adapter = require('axios/lib/adapters/http')

jest.setTimeout(100000);

function getByTestId(component, name) {
    return component.root.findByProps({testId: name}).props;
}

//A fake rendered DOM that has all the dependencies for using the QueryContext
function renderTestingComponent(comp) {
    const host = "http://example.com/"
    return renderer.create(
        <GlobalContext.Provider value={{api_host: host}}>
            <QueryContextProvider>
                {comp}
            </QueryContextProvider>
        </GlobalContext.Provider>
    )
}


// A fake component and that uses all the states of the QueryContext so that they can be tested
const TestingComponent = (props) => {
    const {
        data,
        status,
        runQuery,
    } = useContext(QueryContext);

    const performQuery = () => runQuery(props.group.selectedDatasets, props.group);

    return (
        <>
            <Button testId={"query-button-refetch"}
                    handleClick={performQuery} styling={["primary"]}/>
            <h1 testId={"status"}>{status}</h1>
            <p testId={"data"}>{data}</p>
        </>
    );
};

async function simulateQueryButtonClick(component) {
    await act(async () => {
        await getByTestId(component, "query-button-refetch").handleClick()
    });
}

describe("fetchAllData in test component with query context", () => {
    const skyView = {
        ra: 2,
        selectedDatasets: [{"marvel": {uri: "x-men"}}]
    }

    it("Initial state (no performed query)", async () => {
        const component = renderTestingComponent(<TestingComponent group={skyView}/>)
        const statusText = getByTestId(component, "status").children
        expect(statusText).toBe(QueryStatuses.idle);
        const dataContent = getByTestId(component, "data").children
        expect(dataContent).toStrictEqual([]);
    });
    it("Single successful query", async () => {
        nock('http://example.com')
            .get('/query/query/?dataset_uri=x-men&ra=2')
            .reply(200, {
                results: ["wolverine"]
            });

        const component = renderTestingComponent(<TestingComponent group={skyView}/>)
        await simulateQueryButtonClick(component);
        await act(() => waitFor(() => expect(getByTestId(component, "status").children).toBe(QueryStatuses.success)));
        const dataContent = getByTestId(component, "data").children
        expect(dataContent).toStrictEqual(["wolverine"]);

    });
    it("Multiple successful queries", async () => {
        nock('http://example.com')
            .get('/query/query/?dataset_uri=x-men&ra=2')
            .reply(200, {
                results: ["wolverine"]
            });


        nock('http://example.com')
            .get('/query/query/?dataset_uri=prof-x&ra=2')
            .reply(200, {
                results: ["professor X", "or just the prof"]
            });

        nock('http://example.com')
            .get('/query/query/?dataset_uri=robin&ra=2')
            .reply(200, {
                results: ["sidekick robin"]
            });

        skyView.selectedDatasets = [{"batman": {uri: "robin"}}, {"marvel": {uri: "prof-x"}}]
        const component = renderTestingComponent(<TestingComponent group={skyView}/>)
        await simulateQueryButtonClick(component);
        await act(() => waitFor(() => expect(getByTestId(component, "status").children).toBe(QueryStatuses.success)));
        const dataContent = getByTestId(component, "data").children
        expect(dataContent).toStrictEqual(["sidekick robin", "professor X", "or just the prof"]);
    });
    it("With invalid data", async () => {
        nock('http://example.com')
            .get('/query/query/?dataset_uri=joker&ra=2')
            .reply(200, {
                results: "error; only heroes not villains"
            });

        skyView.selectedDatasets = [{"batman": {uri: "joker"}}]
        const component = renderTestingComponent(<TestingComponent group={skyView}/>)
        await simulateQueryButtonClick(component);
        await act(() => waitFor(() => expect(getByTestId(component, "status").children).toBe(QueryStatuses.success)));
        const dataContent = getByTestId(component, "data").children
        expect(dataContent).toStrictEqual([]);
    });
    it("Two queries; one error, one success", async () => {
        nock('http://example.com')
            .get('/query/query/?dataset_uri=not-existing&ra=2')
            .replyWithError('Invalid parameter given');


        nock('http://example.com')
            .get('/query/query/?dataset_uri=robin&ra=2')
            .reply(200, {
                results: ["sidekick robin"]
            });

        skyView.selectedDatasets = [{"batman": {uri: "robin"}}, {"marvel": {uri: "not-existing"}}]
        let component = renderTestingComponent(<TestingComponent group={skyView}/>)
        await simulateQueryButtonClick(component);
        await act(() => waitFor(() => expect(getByTestId(component, "status").children).toBe(QueryStatuses.success)));
        const dataContent = getByTestId(component, "data").children
        expect(dataContent).toStrictEqual(["sidekick robin"]);
    });
    it("Single errored query (error is only logged; resulting in no data)", async () => {
        nock('http://example.com')
            .get('/query/query/?dataset_uri=x-men&ra=invalid')
            .replyWithError('Invalid parameter given');

        skyView.selectedDatasets = [{"marvel": {uri: "x-men"}}]
        skyView.ra = "invalid"
        const component = renderTestingComponent(<TestingComponent group={skyView}/>)
        await simulateQueryButtonClick(component);
        await act(() => waitFor(() => expect(getByTestId(component, "status").children).toBe(QueryStatuses.success)));
        const dataContent = getByTestId(component, "data").children
        expect(dataContent).toStrictEqual([]);
    });
    it("Single successful query with incorrect format", async () => {
        nock('http://example.com')
            .get('/query/query/?dataset_uri=x-men&ra=incorrect-format')
            .reply(200, {
                results: {test: "results should never be a single object"}
            });

        skyView.ra = 'incorrect-format'
        const component = renderTestingComponent(<TestingComponent group={skyView}/>)
        await simulateQueryButtonClick(component);
        await act(() => waitFor(() => expect(getByTestId(component, "status").children).toBe(QueryStatuses.error)));
        const dataContent = getByTestId(component, "data").children
        expect(dataContent).toEqual("Error combining data from the query results:\ndata.toLowerCase is not a function");

    });
});

