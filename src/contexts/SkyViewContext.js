import React, {createContext, useContext, useState} from "react";
import {UIContext} from "./UIContext";
import {setGroupedSetterValue} from "../utils/Util";
import {QueryContext} from "./QueryContext";

export const SkyViewContext = createContext();


export function SkyViewContextProvider({children}) {
    const {
        skyView,
        setSkyView,
    } = useContext(UIContext);

    const {runQuery} = useContext(QueryContext)

    // if set to true, the Skyview results page will maximize
    const [maximizeResults, setMaximizeResults] = useState(false)

    // a data object selected in list of fetched data
    const [selectedDataItem, setSelectedDataItem] = useState(undefined)

    // an Aladin object selected in the Aladin view
    const [skyObject, setSkyObject] = useState(undefined)

    // ESASky
    const [message, setMessage] = useState({})
    const [refreshESASky, setRefreshESASky] = useState(false)

    function goToClick(object) {
        setNewFov([2, 1])
        let radec = [object.ra, object.dec]
        setNewSkyCoords(radec)

        // construct a skyobject in the Aladin format to represent in the DataProduct panel
        let skyObject = {}
        try {
            skyObject.name = object.name
            skyObject.thumbnail = object.thumbnail
            skyObject.download = object.url
            skyObject.collection = object.collection
            skyObject.type = object.dataproduct_type
            skyObject.level = object.level
            setSkyObject(skyObject)
        } catch (e) {
            console.log('object not found')
        }
    }


    // this function not only sets the new SkyCoords,
    // but also triggers a reload of the data when the SkyCoords have changed.
    const setNewSkyCoords = (newSkyCoords) => {
        let x1 = parseFloat(skyView.ra)
        let x2 = parseFloat(newSkyCoords[0])
        let y1 = parseFloat(skyView.dec)
        let y2 = parseFloat(newSkyCoords[1])

        let move_factor_x = Math.abs((x2 - x1) / skyView.fov)
        let move_factor_y = Math.abs((y2 - y1) / skyView.fov)

        // refresh conditionally
        if ((move_factor_x > skyView.refreshFactor) || (move_factor_y > skyView.refreshFactor)) {
            // setSkyCoords(newSkyCoords)
            setGroupedSetterValue(setSkyView, "ra", newSkyCoords[0]);
            setGroupedSetterValue(setSkyView, "dec", newSkyCoords[1]);
            runQuery(skyView?.selectedDatasets, skyView);
        }
    }

    // post a new message to the ESASky frame
    const sendMessageToESASky = (newMessage) => {
        setMessage(newMessage)
        setRefreshESASky(!refreshESASky)
    }

    const setNewFov = (newFov) => {
        // don't be too sensitive about refreshing when zooming in/out
        let old_fov = Math.round(parseFloat(skyView.fov))
        let new_fov = Math.round(parseFloat(newFov[0]))

        // refresh conditionally
        let zoom_factor = Math.min(old_fov, new_fov) / Math.max(old_fov, new_fov)

        if (zoom_factor < skyView.refreshFactor) {
            // only use the x-axies of fov
            setGroupedSetterValue(setSkyView, "fov", newFov[0]);
            runQuery(skyView?.selectedDatasets, skyView);
        }
    }

    return (
        <SkyViewContext.Provider
            value={{
                setNewSkyCoords,
                setNewFov,
                maximizeResults,
                setMaximizeResults,
                selectedDataItem,
                setSelectedDataItem,
                skyObject,
                setSkyObject,
                message,
                setMessage,
                refreshESASky,
                setRefreshESASky,
                sendMessageToESASky,
                goToClick,
            }}
        >
            {children}
        </SkyViewContext.Provider>
    )
}
