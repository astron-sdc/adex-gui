import React from "react";
import {toDecimalsLabel, toHMSLabel, parseRaDecToDecimals} from "./coordinates";
import expect from "expect";

//HMS/DMS label = "hh mm sss, +/-dd dd.ddd" format
//input format: RA => ddd.ddd [,| |+|-]^1 dd.ddd <= dec
//output format: RA (hh mm ss.sss) dec (dd mm ss.ss)
describe("Represent RA,dec coordinates as 'HH MM SS.SS' string", () => {
    it('empty input results in error label', () => {
        const input = "";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "error";
        expect(actualOutput).toEqual(expectedOutput);
    });
    it('Decimal degrees input missing comma in HMS/DMS label', () => {
        const input = "123.456 -12.345";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "error";
        expect(actualOutput).toEqual(expectedOutput);
    });
    it('Decimal degrees input separated by comma results in HMS/DMS label', () => {
        const input = "123.456,-12.345";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "08 13 49.44, -12 20 42.00";
        expect(actualOutput).toEqual(expectedOutput);
    });
    it('Decimal degrees input separated by comma and space results in HMS/DMS label', () => {
        const input = "123.456, -12.345";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "08 13 49.44, -12 20 42.00";
        expect(actualOutput).toEqual(expectedOutput);
    });
    it('Decimal degrees input separated by comma, space with plus results in HMS/DMS label', () => {
        const input = "123.456, +12.345";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "08 13 49.44, +12 20 42.00";
        expect(actualOutput).toEqual(expectedOutput);
    });
    it('Decimal degrees input separated by comma, with plus results in HMS/DMS label', () => {
        const input = "123.456,+12.345";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "08 13 49.44, +12 20 42.00";
        expect(actualOutput).toEqual(expectedOutput);
    });
    it('Decimal degrees faulty input results in error label', () => {
        const input = "123.456, ++12.345";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "error";
        expect(actualOutput).toEqual(expectedOutput);
    });
    it("HMS/DMS as input to HMS/DMS as string (remains same)", () => {
        const input = "08 13 49.44, -12 20 42.00";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "08 13 49.44, -12 20 42.00";
        expect(actualOutput).toEqual(expectedOutput);
    });
    it("HMS/DMS with + sign as input to HMS/DMS as string (remains same)", () => {
        const input = "08 13 49.44, +12 20 42.00";
        let actualOutput = toHMSLabel(input);
        const expectedOutput = "08 13 49.44, +12 20 42.00";
        expect(actualOutput).toEqual(expectedOutput);
    });
});


describe("Parse RA dec to decimals as floats", () => {
    it('empty input results in empty coordinates (empty array)', () => {
        const input = "";
        let actualOutput = parseRaDecToDecimals(input);
        const expectedOutput = [];
        expect(actualOutput).toEqual(expectedOutput);
    });
    it('HMS string input with minus sign results in array with decimal coordinates as float', () => {
        const input = "08 13 49.44, -12 20 42.00";
        let actualOutput = parseRaDecToDecimals(input);
        const expectedOutput = [123.456,-12.345];
        expect(actualOutput).toEqual(expectedOutput);
    });
    it('HMS string input without a sign results in array with decimal coordinates as float', () => {
        // example: "08 13 49.44, -12 20 42.00" => "123.456, -12.345"
        const input = "08 13 49.44, 12 20 42.00";
        let actualOutput = parseRaDecToDecimals(input);
        const expectedOutput = [123.456,12.345];
        expect(actualOutput).toEqual(expectedOutput);
    });
});

describe("Represent RA,dec coordinates as decimal degrees as string", () => {
    it('empty input results in', () => {
        const input = "";
        let actualOutput = toDecimalsLabel(input);
        const expectedOutput = "error";
        expect(actualOutput).toEqual(expectedOutput);
    });
});