import renderer from "react-test-renderer";
import {MemoryRouter} from "react-router-dom";
import React from "react";

export function isEmptyObject(obj) {
    return obj === undefined || (obj
        && Object.keys(obj).length === 0
        && Object.getPrototypeOf(obj) === Object.prototype)
}


//TEST UTILS
export function renderElement(element) {
    return renderer.create(element).toJSON();
}

export function renderElementWithRouter(element) {
    const component = renderer.create(
        <MemoryRouter>
            {element}
        </MemoryRouter>
    );
    return component.toJSON();
}

export function isEmptyString(string) {
    return string === undefined || string === ""
}

export function findAllOfType(element, type) {
    let queue = [element];
    let visitedNodes = [];
    let typeNodes = [];

    while (queue.length > 0) {
        let currentNode = queue.pop();
        if (!visitedNodes.includes(currentNode)) {
            visitedNodes.push(currentNode);

            if (currentNode.type === type) {
                typeNodes.push(currentNode);
            }

            let children = currentNode?.children;
            if (children instanceof Array) {
                const mergedChildren = [].concat.apply([], children);
                queue = queue.concat(mergedChildren);
            } else if (children instanceof Object) {
                queue.push(children);
            }
        }
    }
    return typeNodes;
}

export function findAllIds(element) {
    let queue = [element];
    let visitedNodes = [];
    let ids = [];

    while (queue.length > 0) {
        let currentNode = queue.pop();
        if (!visitedNodes.includes(currentNode)) {
            visitedNodes.push(currentNode);

            if (currentNode.props?.id !== undefined) {
                ids.push(currentNode);
            }

            let children = currentNode?.children;
            if (children instanceof Array) {
                const mergedChildren = [].concat.apply([], children);
                queue = queue.concat(mergedChildren);
            } else if (children instanceof Object) {
                queue.push(children);
            }
        }
    }
    return ids;
}

export function getGroupedSetterFunction(groupedSetter, objectName) {
    return (value) => setGroupedSetterValue(groupedSetter, objectName, value);
}

export function setGroupedSetterValue(groupedSetter, objectName, value) {
    groupedSetter(prev => (
        {
            ...prev,
            [objectName]: value
        }
    ));
}

/**
 * @param groupedSetter the use state objectName for setting values within a group
 * example: setSkyView
 * @param objectName the name of the objectName within the grouped setter as a string
 * example: "ra" for skyView.ra
 * @param dependencyObject the central object that contains all information for all dependent fields
 * example: skyView.datasets contains all information about datasets with all fields (like 'archive')
 * @param changedDependencyObject the changed central object that contains all selected information
 * example: skyView.selectedDatasets has the partial set of skyView.datasets
 * @param dependencyObjectName the name of the dependency object as described in the group that changes.
 * example: you changed the field 'archive' so that changes the selectedDatasets object
 * @param oldValue the previous value of the object that will be changed
 * @param dependencyObjectFieldName [optional] if within the dependency object the field name does not match with the group's field name
 * example: skyView.frequencyMin has the name 'frequencyMin' where as the datasets object uses 'f_min'
 * @return {function(*): void}
 */
export function getGroupedSetterWithSetObjectDependencyFunction(groupedSetter, objectName, dependencyObject, changedDependencyObject, dependencyObjectName, oldValue, dependencyObjectFieldName = undefined) {
    return (value) => setGroupedSetterValueWithSetObjectDependency(groupedSetter, objectName, value, dependencyObject, changedDependencyObject, dependencyObjectName, oldValue, dependencyObjectFieldName)
}

export function getGroupedSetterWithRangeObjectDependencyFunction(groupedSetter, objectName, dependencyObject, dependencyObjectName, dependencyObjectFieldName, isChangedMin = false) {
    return (value) => setGroupedSetterValueWithRangeObjectDependency(groupedSetter, objectName, value, dependencyObject, dependencyObjectName, dependencyObjectFieldName, isChangedMin)
}

export

function shouldExclude(newValue, oldValue) {
    return newValue.length < oldValue.length || typeof newValue === "string";
}

function getNewSetValuesForObject(object, objectFieldName, newValue, oldValue, changedObject) {
    if (shouldExclude(newValue, oldValue)) {
        const intersectionValues = intersection(newValue, oldValue);
        return filterObjectOnValues(changedObject, intersectionValues);
    } else {
        const unionValues = union(newValue, oldValue)
        return filterObjectOnValues(object, unionValues);
    }
}

//objectFieldName is the 'minimum' when the newValue is a new maximum
//objectFieldName is the 'maximum' when the newValue is a new minimum
function getNewRangeValuesForObject(object, objectFieldName, newValue, isChangedMin) {
    return object.filter(obj => {
        const fieldObject = Object.values(obj)[0][objectFieldName];
        if (isChangedMin) {
            return fieldObject > newValue; //object's maximum value is outside new minimum value

        }
        return fieldObject < newValue;  //object's minimum value is outside new maximum value
    });
}

export function setGroupedSetterValueWithRangeObjectDependency(groupedSetter, objectName, newValue, initialDependencyObject, dependencyObjectName, dependencyObjectFieldName, isChangedMin = false) {
    setGroupedSetterValue(groupedSetter, objectName, newValue);
    setGroupedSetterValue(groupedSetter, 'status', "manual change");
    let newDependentObjectValues = getNewRangeValuesForObject(initialDependencyObject, dependencyObjectFieldName, newValue, isChangedMin);
    setGroupedSetterValue(groupedSetter, dependencyObjectName, newDependentObjectValues);
}

export function setGroupedSetterValueWithSetObjectDependency(groupedSetter, objectName, newValue, initialDependencyObject, changedDependencyObject, dependencyObjectName, oldValue, dependencyObjectFieldName = undefined) {
    setGroupedSetterValue(groupedSetter, objectName, newValue);

    if (!dependencyObjectFieldName) {
        dependencyObjectFieldName = objectName;
    }

    let newDependentObjectValues = getNewSetValuesForObject(initialDependencyObject, dependencyObjectFieldName, newValue, oldValue, changedDependencyObject);
    setGroupedSetterValue(groupedSetter, dependencyObjectName, newDependentObjectValues);
}

export function intersection(array1, array2) {
    return array1.filter(value => array2.indexOf(value) !== -1);
}

export function union(array1, array2) {
    return [...new Set([...array1, ...array2])]
}

export function filterObjectOnValues(object, values) {
    const contains = (array1, array2) => array1.length > array2.length
        ? array2.some(value => array1.includes(value))
        : array1.some(value => array2.includes(value))

    return object.filter(group => {
        const groupValues = Object.values(Object.values(group)[0]).flat();
        return contains(groupValues, values);
    });
}


