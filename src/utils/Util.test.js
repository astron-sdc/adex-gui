import {
    findAllOfType,
    renderElement,
    setGroupedSetterValue,
    setGroupedSetterValueWithSetObjectDependency, setGroupedSetterValueWithRangeObjectDependency
} from "./Util";
import expect from "expect";
import React, {useState} from "react";
import {act, renderHook} from "@testing-library/react-hooks";

describe("findAllOfType", () => {
    const jsonHtml = renderElement(
        <div key="1">
            <div key="2">
                <div key="3">
                    <div key="4">
                        <label key="l1"/>
                        <select>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">
                                <div key="5">C</div>
                            </option>
                        </select>
                    </div>
                    <label key="l2"/>
                </div>
                <p>
                    <div key="6">
                        <label key="l3"/>
                    </div>
                </p>
            </div>
        </div>);
    it("find all divs", () => {
        expect(findAllOfType(jsonHtml, "div").length).toBe(6);
    });
    it("find all labels", () => {
        expect(findAllOfType(jsonHtml, "label").length).toBe(3);
    });
    it("find all options", () => {
        expect(findAllOfType(jsonHtml, "option").length).toBe(3);
    });
    it("find none when not present", () => {
        expect(findAllOfType(jsonHtml, "a").length).toBe(0);
    });
});

describe("setGroupedSetterValue", () => {
    function useSuperHeroesState() {
        const [superHeroes, setSuperHeroes] = useState({
            wolverine: true,
            batman: "black"
        });
        return {superHeroes, setSuperHeroes};
    }

    it("set the single object", () => {
        const {result} = renderHook(() => useSuperHeroesState());
        act(() => {
            setGroupedSetterValue(result.current.setSuperHeroes, "wolverine", false);
        });
        expect(result.current.superHeroes.wolverine).toBe(false);

    });
    it("when object is previously unknown", () => {
        const {result} = renderHook(() => useSuperHeroesState());
        act(() => {
            setGroupedSetterValue(result.current.setSuperHeroes, "superman", true)
        });
        expect(result.current.superHeroes.superman).toBeDefined();
    });
    it("when changing object type", () => {
        const {result} = renderHook(() => useSuperHeroesState());
        const typeChange = "from bool to string";
        act(() => {
            setGroupedSetterValue(result.current.setSuperHeroes, "wolverine", typeChange);
        });
        expect(result.current.superHeroes.wolverine).toBe(typeChange);
    });
})

describe("handleChangeWithDependency", () => {
    const datasets = [
        {
            "superheroOne": {
                "superhero": "wolverine",
                "impact_min": 37,
                "impact_max": 73,
                "weapons": ["sword", "claws", "gun"],
            },
        },
        {
            "superheroTwo": {
                "superhero": "batman",
                "impact_min": 83,
                "impact_max": 91,
                "weapons": ["gun", "bazooka"],
            },
        }
    ]

    function useSuperHeroState() {
        const [superHeroes, setSuperHeroes] = useState({
            names: ["wolverine", "batman"],
            impactMin: 37,
            impactMax: 91,
            weapons: ["gun", "claws", "sword", "bazooka"],
            datasets: datasets,
            selectedDatasets: datasets
        });
        return {superHeroes, setSuperHeroes};
    }

    it("change on list field with matching field names", () => {
        const {result} = renderHook(() => useSuperHeroState());
        const changedValue = ["gun"]
        act(() => {
            setGroupedSetterValueWithSetObjectDependency(result.current.setSuperHeroes, "weapons", changedValue, datasets, result.current.superHeroes.selectedDatasets, 'selectedDatasets', result.current.superHeroes.weapons);
        });
        expect(result.current.superHeroes.selectedDatasets).toStrictEqual(datasets);
        expect(result.current.superHeroes.weapons).toBe(changedValue);
    });
    it("change on minimum range field that excludes an object (not matching field names); ", () => {
        const {result} = renderHook(() => useSuperHeroState());
        const changedValue = 75;
        act(() => {
            setGroupedSetterValueWithRangeObjectDependency(result.current.setSuperHeroes, "impactMin", changedValue, datasets, 'selectedDatasets', "impact_max", true);
        });
        expect(result.current.superHeroes.selectedDatasets).toStrictEqual([datasets[1]]);
        expect(result.current.superHeroes.impactMin).toBe(changedValue);
    });
    it("change on maximum range field that excludes an object (not matching field names)", () => {
        const {result} = renderHook(() => useSuperHeroState());
        const changedValue = 80;
        act(() => {
            setGroupedSetterValueWithRangeObjectDependency(result.current.setSuperHeroes, "impactMax", changedValue, datasets, 'selectedDatasets', "impact_min");
        });
        expect(result.current.superHeroes.selectedDatasets).toStrictEqual([datasets[0]]);
        expect(result.current.superHeroes.impactMax).toBe(changedValue);
    });
    it("changes on maximum and minimum number fields without exclusion (not matching field names)", () => {
        const {result} = renderHook(() => useSuperHeroState());
        const changedValueMin = 50;
        const changedValueMax = 85;
        act(() => {
            setGroupedSetterValueWithRangeObjectDependency(result.current.setSuperHeroes, "impactMin", changedValueMin, datasets, 'selectedDatasets', "impact_max", true);
            setGroupedSetterValueWithRangeObjectDependency(result.current.setSuperHeroes, "impactMax", changedValueMax, datasets, 'selectedDatasets', "impact_min");
        });
        expect(result.current.superHeroes.selectedDatasets).toStrictEqual(datasets);
        expect(result.current.superHeroes.impactMin).toBe(changedValueMin);
        expect(result.current.superHeroes.impactMax).toBe(changedValueMax);
    });
    it("change twice with matching field names", () => {
        const {result} = renderHook(() => useSuperHeroState());
        let changedValue = ["bazooka"]
        act(() => {
            setGroupedSetterValueWithSetObjectDependency(result.current.setSuperHeroes, "weapons", changedValue, datasets, result.current.superHeroes.selectedDatasets, 'selectedDatasets', result.current.superHeroes.weapons);
        });
        expect(result.current.superHeroes.selectedDatasets).toStrictEqual([datasets[1]]);
        expect(result.current.superHeroes.weapons).toBe(changedValue);
        changedValue = ["sword", "gun", "bazooka"]
        act(() => {
            setGroupedSetterValueWithSetObjectDependency(result.current.setSuperHeroes, "weapons", changedValue, datasets, result.current.superHeroes.selectedDatasets, 'selectedDatasets', result.current.superHeroes.weapons);
        });
        expect(result.current.superHeroes.selectedDatasets).toStrictEqual(datasets);
        expect(result.current.superHeroes.weapons).toBe(changedValue);
    });
})