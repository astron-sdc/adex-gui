import React from "react";
import expect from "expect";
import HorizontalMenu from "./HorizontalMenu";
import VerticalMenu from "./VerticalMenu";
import {renderElementWithRouter} from "../utils/Util";

function checkRenderedElement(object, type, prop) {
    expect(object.type).toBe(type);
    expect(object.props).toHaveProperty(prop);
}

describe("Test rendered page of HorizontalMenu", () => {
    it("should have navbar list elements in the correct order", () => {
        const navBar = renderElementWithRouter(<HorizontalMenu/>);

        checkRenderedElement(navBar, 'nav', 'className')
        let orderedList = navBar.children.find(child => child.type === "ol")
        orderedList.children.forEach(child => {
            checkRenderedElement(child, "li", 'className');
        })
    });
});

describe("Test rendered page of VerticalMenu", () => {
    const menu = renderElementWithRouter(<VerticalMenu/>);

    it("should have navbar list elements in the correct order", () => {
        checkRenderedElement(menu, 'aside', 'className')
        let navBar = menu.children.find(child => child.type === "nav")
        let list = navBar.children.find(child => child.type === "ul")

        list.children.forEach(child => {
            let listElement = child.children.find(c => c.type === "li")
            expect(listElement).toBeDefined();
            expect(listElement.props.title).toBeDefined();
        })
    });

    it("should have navbar routing elements for every page (expect Home)", () => {
        checkRenderedElement(menu, 'aside', 'className')
        let navBar = menu.children.find(child => child.type === "nav")
        let list = navBar.children.find(child => child.type === "ul")
        let routingElements = list.children;

        //this assert makes sure that you have added/deleted a page when fully awake. If so, in/decrease counter
        expect(routingElements.length).toBe(2);

        routingElements.forEach(routingElement => {
            checkRenderedElement(routingElement, "a", "href");
        });
    })
});