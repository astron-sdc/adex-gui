import React from "react";
import {Link} from "react-router-dom";
import {isEmptyString} from "../utils/Util";
import {getPages} from "../components/StaticConfig";

export default function VerticalMenu(props) {
    let {activePage, handleShowMenu} = props;

    function getPageElement(page) {
        const pageName = page.name;

        if (isEmptyString(pageName) || isEmptyString(page.icon)) {
            return null;
        }

        let isActive = "";
        if (activePage !== undefined) {
            isActive = page.name === activePage.name ? "active" : "";
        }

        return (
            <Link key={pageName} className="custom-router-link" as={Link}
                  to={isActive !== "" ? '#' : "/" + pageName.toLowerCase()}>
                <li id={pageName} key={pageName}
                    className={"hovering v-navbar-list__item " + isActive}
                    title={pageName}
                    onClick={isActive !== "" ? handleShowMenu : undefined}>
                    <span className={"v-navbar-list__link custom-icon icon icon--medium icon--" + page.icon}/>
                </li>
            </Link>)
    }

    return (
        <aside className="v-navbar v-navbar--dark custom-navbar custom-navbar--v-bar">
            <nav className="v-navbar-list">
                <ul className="v-navbar-list__wrapper v-navbar-list__wrapper--top">
                    {getPages().map(page => getPageElement(page))}
                </ul>
            </nav>
        </aside>
    );
}