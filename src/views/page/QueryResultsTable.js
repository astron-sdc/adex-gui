import React, {useContext, useMemo} from "react";
//import Spinner from "../basics/Spinner";
import LoadingSpinner from '../../components/LoadingSpinner';
import Bar from "../basics/Bar";
import {SkyViewContext} from "../../contexts/SkyViewContext";
import Icon from "../basics/Icon";
import Button from "../basics/Button";
import Badge from "../basics/Badge";
import {QueryStatuses} from "../../contexts/QueryContext";

function hasColumn(data, type) {
    return data.some(dataRow => Object.keys(dataRow).includes(type));
}

function createCell(isVisibile, size, value, extraClasses) {
    return isVisibile ? <div className={"table__cell table__cell--" + size + " " + extraClasses}>{value}</div> : null
}

function Header(props) {
    return <div className="table__header">
        <div className="table__row">
            {createCell(hasColumn(props.data, "name"), "medium", "Name")}
            {createCell(hasColumn(props.data, "dataset"), "medium-small", "Dataset")}
            {createCell(hasColumn(props.data, "collection"), "medium-small", "Collection")}
            {createCell(hasColumn(props.data, "level"), "medium-mini", "Level")}
            {createCell(hasColumn(props.data, "target"), "medium-small", "Target")}
            {createCell(hasColumn(props.data, "dataproduct_type") || hasColumn(props.data, "dataproductType"),
                "medium-small", "Data Product Type")}
            {createCell(hasColumn(props.data, "dataproduct_subtype") || hasColumn(props.data, "dataproductSubtype"),
                "small", "Data Product Subtype")}
            {createCell(hasColumn(props.data, "datasetID"), "small", "Dataset ID")}
            {createCell(hasColumn(props.data, "ra"), "small", "Ra")}
            {createCell(hasColumn(props.data, "dec"), "small", "Dec")}
            {createCell((hasColumn(props.data, "dec") && hasColumn(props.data, "ra")) && props.hasGoTo, "small", "GoTo")}
            {createCell(hasColumn(props.data, "url"), "small", "Download")}
            {createCell(hasColumn(props.data, "thumbnail"), "small", "Preview")}
        </div>
    </div>;
}

function Row(props) {
    return <div className="table__row table__row--no-spacing">
        {createCell(hasColumn(props.data, "name"), "medium", props.result.name, "table__cell--border-right")}
        {createCell(hasColumn(props.data, "dataset"), "medium-small", props.result.dataset, "table__cell--border-right")}
        {createCell(hasColumn(props.data, "collection"), "medium-small", props.result.collection, "table__cell--border-right")}
        {createCell(hasColumn(props.data, "level"), "medium-mini", props.result.level, "table__cell--border-right")}
        {createCell(hasColumn(props.data, "target"), "medium-small", props.result.target, "table__cell--border-right")}
        {createCell(hasColumn(props.data, "dataproduct_type") || hasColumn(props.data, "dataproductType"), "medium-small",
            props.result.dataproduct_type === undefined ? props.result.dataproductType : props.result.dataproduct_type, "table__cell--border-right")}
        {createCell(hasColumn(props.data, "dataproduct_subtype") || hasColumn(props.data, "dataproductSubtype"), "small",
            props.result.dataproduct_type === undefined ? props.result.dataproductSubtype : props.result.dataproduct_type, "table__cell--border-right")}
        {createCell(hasColumn(props.data, "datasetID"), "small", props.result.datasetID, "table__cell--border-right")}
        {createCell(hasColumn(props.data, "ra"), "small", props.result.ra?.toFixed(2), "table__cell--border-right")}
        {createCell(hasColumn(props.data, "dec"), "small", props.result.dec?.toFixed(2), "table__cell--border-right")}
        {createCell(props.hasGoTo, "small", <Button
            info={"Set the position of this object as the ra/dec and go to the sky object"}
            handleClick={() => props.goToClick(props.result)}
            icon={<Icon name={"arrows"}/>}
            styling={["primary", "icon-button"]}/>, "table__cell--border-right table__cell--horizontal-center")}
        {createCell(hasColumn(props.data, "url"), "small", <Button info={"Download data"} isLink={true}
                                                                   href={props.result.url}
                                                                   styling={["primary", "icon-button"]}
                                                                   icon={<Icon
                                                                       name={"arrow-to-bottom"}/>}/>, "table__cell--border-right table__cell--horizontal-center")}
        {createCell(hasColumn(props.data, "thumbnail"), "small", <Button
            info={"Show thumbnail. Open in new tab to keep current query results"} isLink={true}
            href={props.result.thumbnail} styling={["primary", "icon-button"]}
            icon={<Icon name={"image"}/>}/>, "table__cell--horizontal-center")}
    </div>;
}

function TableResults(props) {
    function createTable(data) {
        return <div className={"table__content table__content--scroll table__content--scroll-" + props.size}>
            {data.map(dataRow => <Row
                key={"row-" + data.indexOf(dataRow)}
                data={data}
                result={dataRow}
                hasGoTo={props.hasGoTo}
                goToClick={props.goToClick}/>)
            }
        </div>;
    }
    return useMemo(() => createTable(props.data), [props.data]);
}


export default function QueryResultsTable(props) {
    const {goToClick} = useContext(SkyViewContext);
    const {name, status, idleMessage, data, totalDataSize} = props;

    if (status === QueryStatuses.idle) {
        return (<Bar key={"idle-bar-" + idleMessage} message={idleMessage}
                     isFixed={false} icon={"info"} extraClasses={"popup-bar--color-inherit"}/>);
    }

    if (status === QueryStatuses.loading) {
        return <LoadingSpinner/>
        //return <Spinner message={"Loading Query"} size={"h3"}/>;
    }

    if (data.length === 0) {
        return <div>No results</div>
    }

    const badgeMessage = totalDataSize > data.length ? data.length + "/" + totalDataSize : data.length

    return <div className="table">
        <h2>Query results
            <Badge message={badgeMessage} isCounter={true} isLarge={true}></Badge>
        </h2>
        <Header key={name + "-table-header"} data={data} hasGoTo={props.hasGoTo}/>
        <TableResults key={name + "-table-results"}
                      data={data} size={props.size}
                      hasGoTo={props.hasGoTo}
                      goToClick={goToClick}/>
    </div>;
}