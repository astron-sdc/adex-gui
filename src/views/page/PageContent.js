import React from "react";

export default function PageContent(content) {
    return (
        <div className="content-wrapper__inner content-wrapper">
            {content?.content}
        </div>
    );
}