import React from "react";
import expect from "expect";
import renderer from 'react-test-renderer';
import PageContent from "./PageContent";

describe("Test rendered page", () => {
    it("should have base div when there is no page", () => {
        const component = renderer.create(
            <PageContent/>
        );
        const pageContent = component.toJSON();
        expect(pageContent.type).toBe("div");
        expect(pageContent.children).toBeNull();
    });
});
