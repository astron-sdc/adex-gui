import React from "react";
import logo from '../assets/adex-wide.png'; //TODO: use svg
import {NavLink} from "react-router-dom";


export default function HorizontalMenu() {
    return (
        <nav className="h-navbar h-navbar--dark custom-navbar custom-navbar--h-bar">
            <ol className="h-navbar-list">
                <li className="h-navbar-list__item">
                    <NavLink className="h-navbar-logo logo" as={NavLink} to="/">
                        <img className="h-navbar-logo logo" src={logo} alt=""/>
                    </NavLink>
                </li>
                <li className="h1 custom-title--h1" style={{"marginLeft":"30rem", "marginTop":"1rem"}}>ALPHA VERSION</li>
            </ol>
        </nav>
    );
}