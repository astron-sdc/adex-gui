import React from "react";
import {isEmptyObject, isEmptyString} from "../../../utils/Util";
import Button from "../../basics/Button";
import Icon from "../../basics/Icon";

export default function HeaderElement(header, key) {
    if (isEmptyObject(header)) {
        return null;
    }

    const {title, buttonInfo, buttonTitle, isButtonActive, iconName, handleClick} = header?.header;

    const titleHeader = isEmptyString(title)
        ? null
        : <h1 className="custom-title custom-title--h1">{title}</h1>;

    const iconElement = (iconName === undefined || iconName === "")
        ? null
        : <Icon name={iconName} position={"right"}/>

    const button = (buttonTitle === undefined || buttonTitle === "") && (iconName === undefined || iconName === "")
        ? null
        : <div className="custom-button">
            <Button
                title={buttonTitle}
                info={buttonInfo}
                handleClick={handleClick}
                isDisabled={!isButtonActive}
                disabledInfo={isButtonActive ? undefined : "This action is now disabled"}
                styling={["primary", "small"]}
                icon={iconElement}/>
        </div>

    return (<div key={key} id={"header" + title} className="input__row flex-container">
            {titleHeader}
            {button}
        </div>
    );
}