import {Checkbox} from "../../../models/menuOptions/Checkbox";
import CheckboxesElement from "./CheckboxesElement";
import {Dropdown} from "../../../models/menuOptions/Dropdown";
import DropdownElement from "./DropDownElement";
import {InputField} from "../../../models/menuOptions/InputField";
import InputFieldElement from "./InputFieldElement";
import {RangeSlider} from "../../../models/menuOptions/RangeSlider";
import RangeSliderElement from "./RangeSliderElement";
import React from "react";
import {MultiRowElement} from "./MultiRowElement";
import {MultiRow} from "../../../models/menuOptions/MultiRow";

export default function GetViewElement(element, keyName) {
    switch (element.constructor) {
        case Checkbox:
            return <CheckboxesElement key={"checkboxes-" + keyName + element.title} checkbox={element}/>;
        case Dropdown:
            return <DropdownElement key={"dropdown-" + keyName + element.title} dropdown={element}/>;
        case InputField:
            return <InputFieldElement key={"inputfield-" + keyName + "-" + element.title} inputField={element}/>;
        case RangeSlider:
            return <RangeSliderElement key={"range-slider-" + element.title} rangeSlider={element}/>;
        case MultiRow:
            return <MultiRowElement key={"multi-row" + element.title} multiRow={element}/>
        default:
            console.error("Element added is of an unknown type and therefore nothing is rendered: ", element.constructor, element)
            return null;
    }
}