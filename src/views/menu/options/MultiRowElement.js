import React from "react";
import GetViewElement from "./GetViewElement";
import {isEmptyObject} from "../../../utils/Util";
import Tooltip from "../../basics/Tooltip";

export function MultiRowElement(multiRow) {
    if (isEmptyObject(multiRow)) {
        return null;
    }

    const {title, tooltipInfo, menuOptions} = multiRow?.multiRow

    if (!(menuOptions instanceof Array) || isEmptyObject(title)) {
        return null;
    }

    const content = menuOptions.map(menuOption => GetViewElement(menuOption, "multiRow-column" + title));

    return <div id={"multi-row-" + title}>
        <h2 className="custom-title custom-title--h2">{title}{<Tooltip message={tooltipInfo}/>}</h2>
        <div>
            <div className="input__row">
                {content}
            </div>
        </div>
    </div>
}