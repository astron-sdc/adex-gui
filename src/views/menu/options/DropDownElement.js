import React from "react";
import {isEmptyObject, isEmptyString} from "../../../utils/Util";
import Spinner from "../../basics/Spinner";
import Tooltip from "../../basics/Tooltip";

function getOptions(option) {
    return <option value={option} key={option}>{option}</option>
}


export default function DropdownElement(dropdown) {
    if (isEmptyObject(dropdown)) {
        return null;
    }

    let {title, titleClass, tooltipInfo, tooltipPosition, values, currentValue, handleChange} = dropdown?.dropdown;
    if (!titleClass) {
        titleClass = "custom-title custom-title--h2";
    }

    const header = isEmptyString(title)
        ? null
        : <h2 className={titleClass}>{title}{<Tooltip message={tooltipInfo} position={tooltipPosition}/>}</h2>

    const content = values === undefined || values.length === 0 || !Array.isArray(values)
        ? <Spinner message={"Retrieving values"}/>
        : <div className="input__row">
            <div id={title}>
                <div className="input-select-wrapper">
                    <label htmlFor={title}/>
                    <select key={"select-" + title}
                            className="input input--select icon-after icon-after--angle-down"
                            id={title}
                            onChange={(event) => handleChange(event.target.value)}
                            value={currentValue}>
                        {values?.map(option => getOptions(option))}
                    </select>
                </div>
            </div>
        </div>

    return (
        <div id={"dropdown-" + title}>
            {header}
            {content}
        </div>
    );
}