import React from "react";
import expect from "expect";
import CheckboxesElement from "./CheckboxesElement";
import DropdownElement from "./DropDownElement";
import {findAllIds, findAllOfType, renderElement} from "../../../utils/Util";
import HeaderElement from "./HeaderElement";
import InputFieldElement, {formatIfNumber} from "./InputFieldElement";
import {typeStylingDict} from "../../../utils/inputFieldTypeStylingDict";
import RangeSliderElement, {calculateFill, calculatePosition} from "./RangeSliderElement";
import {MultiRowElement} from "./MultiRowElement";
import {InputField} from "../../../models/menuOptions/InputField";
import {Dropdown} from "../../../models/menuOptions/Dropdown";

function checkHeaderPresent(element) {
    expect(element.children.find(child => child.type === "h2" || child.type === "h1")).toBeDefined();
}

function checkHeaderNotPresent(element) {
    expect(element.children.find(child => child.type === "h2" || child.type === "h1")).toBeUndefined();
}

describe("Checkboxes", () => {
    it("should have nothing when no checkbox is defined", () => {
        const checkboxElement = renderElement(<CheckboxesElement/>)
        expect(checkboxElement).toBeNull();
    });
    it("should have no title when only values are defined", () => {
        const checkbox = {
            values: {
                "catwoman": "criminal or hero?",
                "superman": "man of steel",
                "hulk": undefined
            },
            checkedValues: [],
        };
        const checkboxElement = renderElement(<CheckboxesElement checkbox={checkbox}/>)
        expect(checkboxElement.children.length).toBe(2);

        checkHeaderNotPresent(checkboxElement);
    });

    it("should have title with loading message and no selectAll option when no values are defined", () => {
        const checkbox = {title: "superheroes", values: {}, checkedValues: [],};
        const checkboxElement = renderElement(<CheckboxesElement checkbox={checkbox}/>);
        checkHeaderPresent(checkboxElement);

        const hasLoadingMessage = checkboxElement.children.some(child => {
            return child.children.pop() === "Retrieving values";
        })
        expect(hasLoadingMessage).toBe(true);

        let contentElements = checkboxElement.children.find(child => child.type === "div");
        expect(contentElements).toBeUndefined();
    });
    it("should have all checkboxElements (select all, 3 checkboxes and 2 tooltips)", () => {
        //TODO: Nico
    });
    it("should have no tooltips when only values are defined", () => {
        const checkbox = {
            title: "superheroes",
            values: {"catwoman": undefined, "superman": undefined},
            checkedValues: [],
        };
        const checkboxElement = renderElement(<CheckboxesElement checkbox={checkbox}/>);

        let labels = findAllOfType(checkboxElement, "label");
        expect(labels.length).toBe(3);
        labels.forEach(label => {
            let inputElement = label.children.find(child => child.type === "input");
            expect(inputElement.children).toBe(null);
        });
    });
    it("should not have select all when only one checkbox is defined", () => {
        const checkbox = {
            title: "superheroes",
            values: {"catwoman": "criminal or hero?"},
            checkedValues: [],
        };
        const checkboxElement = renderElement(<CheckboxesElement checkbox={checkbox}/>);

        let labels = findAllOfType(checkboxElement, "label");
        expect(labels.length).toBe(1);
        const hasSelectAll = labels.some(label => label.props.htmlFor === "selectAll");
        expect(hasSelectAll).toBe(false);
    });
});

describe("Dropdown", () => {
    it("should have nothing when no dropdown is defined", () => {
        const dropdownElement = renderElement(<DropdownElement/>)
        expect(dropdownElement).toBeNull();
    });
    it("should have no title when only values are defined", () => {
        const dropdown = {
            values: ["catwomen", "superman"],
        };
        const dropdownElement = renderElement(<DropdownElement dropdown={dropdown}/>)

        checkHeaderNotPresent(dropdownElement);

        let options = findAllOfType(dropdownElement, "option");
        expect(options.length).toBe(2);
    });
    it("should have title with loading message when no values are defined", () => {
        const dropdown = {title: "superheroes", values: [],};
        const dropdownElement = renderElement(<DropdownElement dropdown={dropdown}/>);
        checkHeaderPresent(dropdownElement);

        const hasLoadingMessage = dropdownElement.children.some(child => {
            return child.children.pop() === "Retrieving values";
        })
        expect(hasLoadingMessage).toBe(true);

        let contentElements = dropdownElement.children.find(child => child.type === "div");
        expect(contentElements).toBeUndefined();
    });
    it("should have all elements (title, 2 selection option)", () => {
        const dropdown = {title: "superheroes", values: ["catwomen", "superman"],};
        const dropdownElement = renderElement(<DropdownElement dropdown={dropdown}/>);
        checkHeaderPresent(dropdownElement)

        let options = findAllOfType(dropdownElement, "option");
        expect(options.length).toBe(2);

    });
});

describe("Header", () => {
    it("should have nothing when no header is defined", () => {
        const headerElement = renderElement(<HeaderElement/>)
        expect(headerElement).toBeNull();
    });
    it("should have no title when only a button is defined", () => {
        const header = {
            buttonTitle: "kill all",
            buttonInfo: "clicking this button will make wolverine kill all",
            iconName: "claws"
        };
        const headerElement = renderElement(<HeaderElement header={header}/>)

        checkHeaderNotPresent(headerElement);
    });
    it("should have no button when only a title is defined", () => {
        const header = {
            title: "wolverine"
        };
        const headerElement = renderElement(<HeaderElement header={header}/>)
        checkHeaderPresent(headerElement);

        let contentElements = headerElement.children.find(child => child.type === "div");
        expect(contentElements).toBeUndefined();
    });
    it("should have no icon in the button when it is not defined", () => {
        const header = {
            title: "wolverine",
            buttonTitle: "kill all",
            buttonInfo: "clicking this button will make wolverine kill all",
        };
        const headerElement = renderElement(<HeaderElement header={header}/>)

        let buttonElement = findAllOfType(headerElement, "button");
        let iconElement = findAllOfType(buttonElement[0], "span");
        expect(iconElement[0]).toBeUndefined();

    });
    it("should have no button title when it is not defined", () => {
        const header = {
            title: "wolverine",
            buttonInfo: "clicking this button will make wolverine kill all",
            iconName: "claws"
        };
        const headerElement = renderElement(<HeaderElement header={header}/>)

        let button = findAllOfType(headerElement, "button");
        expect(button[0].children.length).toBe(1);
    });
    it("should have disabled button info when it is not defined", () => {
        const header = {
            title: "wolverine",
            buttonTitle: "kill all",
            iconName: "claws"
        };
        const headerElement = renderElement(<HeaderElement header={header}/>)

        let button = findAllOfType(headerElement, "button");
        expect(button[0].props.title).toBe("This action is now disabled")
    });
    it("should have all elements (title, button with icon, title and info)", () => {
        //TODO: Nico
    });
});

describe("InputField", () => {
    it("should have nothing when no inputField is defined", () => {
        const inputFieldElement = renderElement(<InputFieldElement/>)
        expect(inputFieldElement).toBeNull();
    });
    it("should have nothing when title s defined (how will you know what you should enter?)", () => {
        const inputField = {
            title: "",
            typeStylingDict: typeStylingDict.text,
        };
        const inputFieldElement = renderElement(<InputFieldElement inputField={inputField}/>)
        expect(inputFieldElement).toBeNull();
    });
    it("should have default type and styling (=text) when it is not defined", () => {
        const inputField = {
            title: "wolverine",
            currentValue: 0
        };
        const inputFieldElement = renderElement(<InputFieldElement inputField={inputField}/>)
        let inputElement = findAllOfType(inputFieldElement, "input");
        expect(inputElement[0].props.type).toBe("text");
        expect(inputElement[0].props.className).toBe("input ");
    });
    it("should have default type and styling (=text) when it is not found in the typeStylingDict", () => {
        //TODO: Nico. Remove the second OR conditional in class
        // InputFieldElement, line 16 'typeStylingDict.hasOwnProperty(typeStylingTuple)'
        // for it to fail
    });
    it("should format to number for string input", () => {
        const value = formatIfNumber(typeStylingDict.number.type, "ha")
        expect(value).toBe(0.00);
    });
    it("should format to number for empty string input", () => {
        const value = formatIfNumber(typeStylingDict.number.type, "")
        expect(value).toBe(0.00);
    });
    it("should format to 2 decimal number", () => {
        const value = formatIfNumber(typeStylingDict.number.type, 1.259)
        expect(value).toBe(1.26);
    });
    it("should not format for other input types", () => {
        const value = formatIfNumber(typeStylingDict.number.button, 1.259)
        expect(value).toBe(1.259);
    });
        it("should format for leading zeroes", () => {
        const value = formatIfNumber(typeStylingDict.number.button, 1.259)
        expect(value).toBe(1.259);
    });
});

describe("RangeSlider", () => {
    it("should have nothing when no range slider is defined", () => {
        const rangeSliderElement = renderElement(<RangeSliderElement/>)
        expect(rangeSliderElement).toBeNull();
    });
    it("should have nothing when no title is defined (how will you know what you should enter?)", () => {
        const rangeSlider = {
            title: "",
        };
        const rangeSliderElement = renderElement(<RangeSliderElement rangeSlider={rangeSlider}/>)
        expect(rangeSliderElement).toBeNull();
    });
    it("should have all elements (title, 4 input options; min, max, slideMin, slideMax)", () => {
        const rangeSlider = {
            title: "superheroes",
            min: 100,
            max: 1000,
            handleChangeMin: () => undefined,
            handleChangeMax: () => undefined
        };
        const rangeSliderElement = renderElement(<RangeSliderElement rangeSlider={rangeSlider}/>)
        checkHeaderPresent(rangeSliderElement)

        let inputs = findAllOfType(rangeSliderElement, "input");
        expect(inputs.length).toBe(4);
    });
    it("should have currentMin and currentMax set to initial min/max when invalid values", () => {
        const min = 100;
        const max = 1000;
        const rangeSlider = {
            title: "superheroes",
            min: min,
            max: max,
            currentMin: 0,
            currentMax: 2000,
            handleChangeMin: () => undefined,
            handleChangeMax: () => undefined
        };
        const rangeSliderElement = renderElement(<RangeSliderElement rangeSlider={rangeSlider}/>)

        let inputs = findAllOfType(rangeSliderElement, "input");
        const inputsWithCorrectMinMax = inputs.find(input => input.props.max === max && input.props.min === min)
        expect(inputsWithCorrectMinMax).toBeDefined();
    });
    it("should have title with loading message and no slider", () => {
        const min = 100;
        const max = 1000;
        const rangeSlider = {
            title: "superheroes",
            min: min,
            max: max,
            currentMin: 0,
            currentMax: 2000,
        };
        const rangeSliderElement = renderElement(<RangeSliderElement rangeSlider={rangeSlider}/>)
        checkHeaderPresent(rangeSliderElement);

        const hasLoadingMessage = rangeSliderElement.children.some(child => {
            return child.children.pop().includes("Retrieving");
        })
        expect(hasLoadingMessage).toBe(true);

        let contentElements = rangeSliderElement.children.find(child => child.type === "div");
        expect(contentElements).toBeUndefined();
    });
    it("should calculate thumb positions for min/max values", () => {
        const currentMin = 37;
        const currentMax = 73;
        const max = 100;
        const positionLeft = calculatePosition(currentMin, max);
        const expectedPositionLeft = {
            left: "37%",
        }
        const positionRight = calculatePosition(currentMax, max)
        const expectedPositionRight = {
            left: "73%",
        }

        expect(positionLeft).toStrictEqual(expectedPositionLeft);
        expect(positionRight).toStrictEqual(expectedPositionRight);
    });
    it("should calculate range fill for min/max values", () => {
        const currentMin = 0;
        const currentMax = 191;
        const max = 1000;
        const fill = calculateFill(currentMin, currentMax, max);
        const expectedFill = {
            left: "0%",
            right: "80.9%"
        }

        expect(fill).toStrictEqual(expectedFill);
    });
    it("should calculate position and fill with an undefined max value", () => {
        const currentMin = 0;
        const currentMax = 191;
        const max = undefined;
        const fill = calculateFill(currentMin, currentMax, max);
        const expectedFill = {
            left: "0%",
            right: "100%"
        }

        const position = calculatePosition(currentMax, max)
        const expectedPosition = {
            left: "0%",
        }

        expect(fill).toStrictEqual(expectedFill);
        expect(position).toStrictEqual(expectedPosition);
    })
})

describe("MultiRow", () => {
    it("should have nothing when no multiRow is defined", () => {
        const rangeSliderElement = renderElement(<MultiRowElement/>)
        expect(rangeSliderElement).toBeNull();
    });
    it("should have nothing when no title is defined (how will you know what you should enter?)", () => {
        const multiRow = {
            title: "",
        };
        const multiRowElement = renderElement(<MultiRowElement multiRow={multiRow}/>)
        expect(multiRowElement).toBeNull();
    });
    it("should have all elements (title, 2 elements on the same row; inputfield and dropdown)", () => {
        const inputField = new InputField("wolverine");
        inputField.currentValue = 10;
        const dropdown = new Dropdown("batman")
        const multiRow = {
            title: "superheroes",
            menuOptions: [inputField, dropdown]
        };
        const multiRowElement = renderElement(<MultiRowElement multiRow={multiRow}/>)
        checkHeaderPresent(multiRowElement)

        let ids = findAllIds(multiRowElement)
        expect(ids.length).toBe(4);
    });
})