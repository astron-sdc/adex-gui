import React from "react";
import {isEmptyObject, isEmptyString} from "../../../utils/Util";
import Spinner from "../../basics/Spinner";
import Tooltip from "../../basics/Tooltip";

export default function RangeSliderElement(rangeSlider) {
    if (isEmptyObject(rangeSlider)) {
        return null;
    }

    let {
        title,
        tooltipInfo,
        min,
        max,
        currentMin,
        currentMax,
        step,
        handleChangeMin,
        handleChangeMax,
    } = rangeSlider?.rangeSlider;

    if (isEmptyString(title)) {
        return null;
    }

    if (!handleChangeMin || !handleChangeMax || !currentMin === undefined || !currentMax === undefined) {
        return <div>
            <h2 className="custom-title custom-title--h2">{title}{<Tooltip message={tooltipInfo}/>}</h2>
            <Spinner message={"Retrieving range"}/>
        </div>;
    }

    if (min === undefined) {
        min = currentMin;
    }

    if (max === undefined) {
        max = currentMax;
    }

    if (currentMin < min) {
        currentMin = min;
    }
    if (currentMax > max) {
        currentMax = max;
    }

    return <div id={"range-slider-" + title}>
        <h2 className="custom-title custom-title--h2">{title}{<Tooltip message={tooltipInfo}/>}</h2>
        <div className="flex-container-slider">
            <div className="slider-input">
                <label htmlFor="value-min"></label>
                <input className="input input-value" id="value-min" value={currentMin} type="number" step={step}
                       onChange={(event) => handleChangeMin(parseFloat(event.target.value))}></input>
            </div>
            <div slider="" className="custom-slider-styling" id="slider-distance">
                <div>
                    <div inverse-left=""></div>
                    <div inverse-right=""></div>
                    <div range="" style={calculateFill(currentMin, currentMax, max)}></div>
                    <span thumb="" style={calculatePosition(currentMin, max)}></span>
                    <span thumb="" style={calculatePosition(currentMax, max)}></span>
                </div>
                <input id="slider-min" type="range" value={currentMin} max={max} min={min} step={step}
                       onChange={(event) => handleChangeMin(parseFloat(event.target.value))}></input>
                <input id="slider-max" type="range" value={currentMax} max={max} min={min} step={step}
                       onChange={(event) => handleChangeMax(parseFloat(event.target.value))}></input>
            </div>
            <div className="slider-input">
                <label htmlFor="value-max"></label>
                <input className="input input-value" id="value-max" value={currentMax} type="number" step={step}
                       onChange={(event) => handleChangeMax(parseFloat(event.target.value))}></input>
            </div>
        </div>
    </div>
}

function getPercentageValue(current, totalMax) {
    const percentage = current / totalMax * 100;
    if (isNaN(percentage)) {
        return 0;
    }
    return percentage;
}

export function calculatePosition(current, totalMax) {
    let position = {}
    position.left = getPercentageValue(current, totalMax) + "%"
    return position;
}

export function calculateFill(min, max, totalMax) {
    let position = {}
    position.left = getPercentageValue(min, totalMax) + "%";
    position.right = (100 - getPercentageValue(max, totalMax)) + "%";
    return position;
}