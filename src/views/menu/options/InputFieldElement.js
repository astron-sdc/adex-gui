import React from "react";
import {isEmptyObject, isEmptyString} from "../../../utils/Util";
import {typeStylingDict} from "../../../utils/inputFieldTypeStylingDict";
import Spinner from "../../basics/Spinner";
import Tooltip from "../../basics/Tooltip";


export function formatIfNumber(inputFieldType, value) {
    if (inputFieldType === "number") {
        if (typeof value !== "number") {
            value = parseFloat(value);
            if (isNaN(value)) {
                return 0.00;
            }
        }
        return Number(value.toFixed(2));
    }
    return value;
}

export default function InputFieldElement(inputField) {
    if (isEmptyObject(inputField)) {
        return null;
    }

    let {title, tooltipInfo, typeStylingTuple, handleChange, currentValue} = inputField?.inputField;

    if (isEmptyString(title)) {
        return null;
    }

    if (currentValue === undefined) {
        return <Spinner message={"Retrieving defaults"}/>
    }

    if (isEmptyObject(typeStylingTuple) || typeStylingTuple.hasOwnProperty(typeStylingTuple)) {
        typeStylingTuple = typeStylingDict.text;
    }

    let styling = typeStylingTuple.styling !== undefined ? typeStylingTuple.styling : "";
    currentValue = formatIfNumber(typeStylingTuple.type, currentValue);

    return (
        <div className="input__column" id={"input-column-" + title}>
            <label className="input__label" htmlFor={title}>{title}{<Tooltip message={tooltipInfo}/>}</label>
            <input
                className={"input " + styling}
                id={title}
                type={typeStylingTuple.type}
                onChange={(event) => handleChange(formatIfNumber(typeStylingTuple.type, event.target.value))}
                value={currentValue}/>
        </div>

    );
}