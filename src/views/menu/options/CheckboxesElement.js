import React from "react";
import {isEmptyObject, isEmptyString} from "../../../utils/Util";
import Spinner from "../../basics/Spinner";
import Tooltip from "../../basics/Tooltip";

function getCheckBoxElement(content, tooltip, checkedValues, handleChange) {
    const isChecked = checkedValues.indexOf(content) > -1;
    return (
        <div key={content.toString()} className="flex-container flex-container--checkboxes">
            <label htmlFor={content}>
                <input
                    className="input select-all-boxes"
                    tabIndex="0"
                    id={content}
                    name={content}
                    type="checkbox"
                    checked={isChecked}
                    onChange={() => {
                        handleChange(isChecked ? checkedValues.filter(item => item !== content) : checkedValues.concat(content))
                    }}
                />
                {content}
                {<Tooltip message={tooltip}/>}
            </label>
        </div>
    )
}

export default function CheckboxesElement(checkbox, key) {
    if (isEmptyObject(checkbox)) {
        return null;
    }

    const {title, tooltipInfo, values, checkedValues, handleChange} = checkbox?.checkbox;

    if (!Array.isArray(checkedValues) || values.length === 0) {
        return null;
    }

    const header = isEmptyString(title)
        ? null
        : <h2 className="custom-title custom-title--h2">{title}{<Tooltip message={tooltipInfo}/>}</h2>

    const isSelectAll = Object.keys(values).length === checkedValues.length;

    const selectAll = isEmptyObject(values) || Object.keys(values).length <= 1
        ? null
        : <div key="selectAll" className="flex-container">
            <label htmlFor="selectAll">
                <input className="input select-all-boxes"
                       tabIndex="0"
                       id="selectAll"
                       name="selectall"
                       type="checkbox"
                       checked={isSelectAll}
                       onChange={() => handleChange(isSelectAll ? [] : (Array.isArray(values) ? values : Object.keys(values)))}
                />
                Select all
            </label>
        </div>

    const content = isEmptyObject(values)
        ? <Spinner message={"Retrieving values"}/>
        : (Array.isArray(values)
            ? <div className="input__row checkbox-row">
                <div className="input__column">
                    {values?.map(
                        (value) => getCheckBoxElement(value, undefined, checkedValues, handleChange))
                    }
                </div>
            </div>
            : <div className="input__row checkbox-row">
                <div className="input__column">
                    {Object.entries(values)?.map(
                        (valueTooltip) => getCheckBoxElement(valueTooltip[0], valueTooltip[1], checkedValues, handleChange))
                    }
                </div>
            </div>)

    return (<div key={key} id={"checkbox-" + title}>
        {header}
        {selectAll}
        {content}
    </div>);
}