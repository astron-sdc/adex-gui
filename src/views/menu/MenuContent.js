import React from "react";
import HeaderElement from "./options/HeaderElement";
import {isEmptyObject} from "../../utils/Util";
import AdvancedOptions from "../basics/AdvancedOptions";
import GetViewElement from "./options/GetViewElement";

export default function MenuContent(props) {
    const {menu, showMenu} = props

    if (isEmptyObject(menu) || showMenu === undefined) {
        return null;
    }

    return (
        showMenu && <div className="content-wrapper__inner content-wrapper__inner--fixed-width menu-wrapper">
            <HeaderElement key={"header-" + menu?.header?.title} header={menu?.header}/>
            {menu.elements?.map(element => GetViewElement(element, menu.header?.title))}
            {<AdvancedOptions key={"adv-options-" + menu?.header?.title} content={menu.advancedOptions} optionsTitle={"Filters"}/>}
        </div>
    )
};