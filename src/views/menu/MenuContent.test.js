import React from "react";
import expect from "expect";
import renderer from 'react-test-renderer';
import MenuContent from "./MenuContent";
import {findAllIds} from "../../utils/Util";
import {ArchiveMenu, SkyViewMenu} from "../../components/StaticConfig";

describe("Test rendered menu", () => {
    it("should have nothing when there is no menu", () => {
        const component = renderer.create(
            <MenuContent/>
        );
        const menuContent = component.toJSON();
        expect(menuContent).toBeNull();
    });
    it("should contain the skyview menu objects without values/data except checkboxes", () => {
        const component = renderer.create(
            <MenuContent menu={SkyViewMenu} showMenu={true}/>
        );
        const menuContent = component.toJSON();
        //Every menu element has its own id
        let ids = findAllIds(menuContent);
        expect(ids.length).toBeGreaterThan(3)
    });
    it("should contain the search menu objects without values/data except checkboxes", () => {
        const component = renderer.create(
            <MenuContent menu={ArchiveMenu} showMenu={true}/>
        );
        const menuContent = component.toJSON();
        //Every menu element has its own id
        let ids = findAllIds(menuContent);
        expect(ids.length).toBeGreaterThan(0)
    });
    it("should hide the menu", () => {
        const component = renderer.create(
            <MenuContent menu={ArchiveMenu} showMenu={false}/>
        );

        const menuContent = component.toJSON();
        expect(menuContent).toBeNull();
    });
});
