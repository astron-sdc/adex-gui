function getValidObject(allObjects, object) {
    let validObject = allObjects[0];
    if (allObjects.indexOf(object) > -1) {
        validObject = object;
    }
    return validObject;
}

/**
 * A (loading) spinner according to DIAS styling
 * @property message
 *  the (extra) message to display in the spinner
 * @property size
 *  standard is a small one. Options are: p, h1, h2 and h3 (small, big, bigger, biggest)
 * @property type
 *  standard is the primary. Option are: primary, secondary, text and faded (according to mixins settings)
 * @property position
 *  standard is normal. Options are: normal, left and right
 * @return {JSX.Element}
 */
export default function Spinner(props) {
    const {message, size, type, position} = props;
    const allSizes = ["p", "h1", "h2", "h3"];
    const allTypes = ["primary", "secondary", "text", "faded"];
    const allPositions = ["left", "right"];
    const spinnerElement = <span
        className={"spinner image--spinner spinner--" + getValidObject(allTypes, type)
            + "spinner--" + getValidObject(allPositions, position)}>
    </span>

    const validSize = getValidObject(allSizes, size);
    if (validSize === "h1") {
        return <h1 className="center-text-and-icon">{spinnerElement}{message}</h1>
    }
    if (validSize === "h2") {
        return <h2 className="center-text-and-icon">
            {spinnerElement}{message}</h2>
    }
    if (validSize === "h3") {
        return <h3 className="center-text-and-icon">
            {spinnerElement}{message}</h3>
    }
    return <p className="center-text-and-icon">
        {spinnerElement}{message}</p>
}