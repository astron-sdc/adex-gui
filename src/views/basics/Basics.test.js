import renderer from "react-test-renderer";
import expect from "expect";
import React from "react";
import AdvancedOptions from "./AdvancedOptions";
import {findAllOfType} from "../../utils/Util";
import {InputField} from "../../models/menuOptions/InputField";
import {typeStylingDict} from "../../utils/inputFieldTypeStylingDict";
import {Dropdown} from "../../models/menuOptions/Dropdown";
import {Checkbox} from "../../models/menuOptions/Checkbox";
import {Header} from "../../models/menuOptions/Header";

describe("Advanced Options", () => {
    it("should have nothing when the content is undefined", () => {
        const component = renderer.create(
            <AdvancedOptions content={undefined}/>
        );
        const advancedOptionsElement = component.toJSON();
        expect(advancedOptionsElement).toBeNull();
    });
    it("should have a basic div when content is a jsx element", () => {
        const testClassName = "lion-king"
        const content = <div className={testClassName}>Simba and Nale</div>
        const component = renderer.create(
            <AdvancedOptions content={content}/>
        );
        const advancedOptionsElement = component.toJSON();
        const divElements = findAllOfType(advancedOptionsElement[1], "div")
        expect(divElements.length).toBe(3);
        divElements.some(div => div.className === {testClassName})
    });
    it("should create the jsx content when it is an array of MenuOptions", () => {
        const inputField = new InputField("New Lion King Character", undefined, typeStylingDict.text);
        const dropdown = new Dropdown("Select Main Lion");
        dropdown.values = ["Fanna", "Nico"];
        dropdown.currentValue = "Fanna"
        const checkbox = new Checkbox("Select Additional Lions")
        checkbox.checkedValues = ["Simba", "Nala"]
        checkbox.values = ["Simba", "Nala", "Mufasa"]
        const content = [inputField, dropdown, checkbox]
        const component = renderer.create(
            <AdvancedOptions content={content}/>
        );
        const advancedOptionsElement = component.toJSON();
        const divElements = findAllOfType(advancedOptionsElement[1], "div")
        const jsxContent = divElements.find(element => element.props.className === "advanced-options-target");
        expect(jsxContent.children.length).toBe(content.length);
    });
    it("should create content when it is an array of unknown elements (header should always be added first)", () => {
        const content = [new Header("Lion King", undefined, "Play", "Play with character selections", undefined)]
        const component = renderer.create(
            <AdvancedOptions content={content}/>
        );
        const advancedOptionsElement = component.toJSON();
        const divElements = findAllOfType(advancedOptionsElement[1], "div")
        const jsxContent = divElements.find(element => element.props.className === "advanced-options-target");
        expect(jsxContent.children).toBeNull()
    });
});
