import React from "react";
/**
 * A (loading) spinner according to DIAS styling
 * @property message
 *  the (extra) message to display in the popup bar
 * @property isFixed
 *  if true it is shown in the bottom left
 * @property extraClasses
 *  styling classes to add. For example "popup-bar--red" gives 'Error' styling
 *  @property icon
 *  icon you want to show in the pop-up bar (optional)
 * @return {JSX.Element}
 */
import Icon from "./Icon";

export default function Bar(props) {
    const {message, isFixed, extraClasses, icon} = props;

    const iconElement = icon === undefined ? "" : <Icon name={icon}/>

    return <div
        className={"popup-bar " + (isFixed ? "popup-bar--fixed " : "") + (extraClasses !== undefined ? extraClasses : "")}>
        {iconElement}
        {message}
    </div>
}