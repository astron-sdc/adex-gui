import {isEmptyObject} from "../../utils/Util";
import React from "react";

/**
 * An information tooltip according to DIAS styling
 * @property message the message to display after hovering over the info icon
 * @property position the direction which the information should be displayed.
 * Standard is to the right
 * @return {JSX.Element|null}
 * @constructor
 */
export default function Tooltip(props) {
    let {message, position} = props;

    if (isEmptyObject(message)) {
        return null;
    }

    if (!position) {
        position = "bottom"
    }

    return <span className={"tooltip-dias tooltip-dias-" + position} data-tooltip={message}>m</span>;
}