import React from "react";

/**
 * A button according to DIAS styling
 * @property title
 *  the title in the button
 * @property info
 *  the information shown when hovering over the button
 * @property handleClick
 *  function that determines what is to be done
 * @property [styling]
 *  a list of the type(s) and/or color(s) of the button
 *  options are: primary, secondary, red, green, link, readmore, disabled, small and icon-button
 * @property isDisabled [optional]
 *  whether the button is disabled or not
 * @property disabledInfo [optional]
 *  the information shown when hovering over the disabled button
 * @property icon [optional]
 *  dias icon you want to show in button
 * @property isLink
 *  if the button should be displayed as a link
 * @property href
 *  if the button is a link, this is the reference
 * @return {JSX.Element}
 */
export default function Button(props) {
    let {title, info, handleClick, styling, isDisabled, disabledInfo, icon, isLink = false, href} = props;

    let buttonClassName = "button "
    if (styling) {
        styling = styling.map(style => "button--" + style).join(" ");
        buttonClassName = buttonClassName + styling
    }

    if (isDisabled) {
        info = disabledInfo
        buttonClassName = buttonClassName + " button--disabled"
    }

    if (isLink && href !== undefined) {
        return <a className={buttonClassName} href={href}>
            {icon}
        </a>
    }
    return <button className={buttonClassName}
                   title={info}
                   onClick={handleClick}>
        {title}
        {icon}
    </button>
}