import {v1 as uuid} from 'uuid';
import React from "react";
import GetViewElement from "../menu/options/GetViewElement";
import {isEmptyObject} from "../../utils/Util";

/**
 * Advanced options toggle with content according to DIAS styling
 * @property content
 *  the element(s) that should be shown or hidden after toggling.
 *      it can either be a JSX.Element or a list of MenuOptions
 * @property optionsTitle
 *  The title for the toggle. Standard is set to "Advanced options"
 * @return {JSX.Element}
 */
export default function AdvancedOptions(props) {
    let {content, optionsTitle} = props

    if (!content) return null;

    let jsxContent = content
    if (content instanceof Array) {
        jsxContent = <>{content?.map(element => {
            return GetViewElement(element, "advanced-options-")
        })}</>;
    }

    if (isEmptyObject(jsxContent)) {
        return null;
    }

    if (!optionsTitle) optionsTitle = "Advanced options"

    const id = uuid();

    return <>
        <input type="checkbox" className="advanced-options-toggle advanced-options-box" id={id}/>
        <div className="advanced-options-wrapper "><h2
            className="custom-title custom-title--h2 advanced-options-link">{optionsTitle}</h2>
            <label htmlFor={id} className="advanced-options-toggle advanced-options-icon"> </label>
            <div className="advanced-options-target">{jsxContent}</div>
        </div>
    </>
}

