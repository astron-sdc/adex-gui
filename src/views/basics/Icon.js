import React from "react";

/**
 * An icon according to DIAS styling
 * @property name
 *  name of the icon you want to show in button
 * @property position [optional]
 *  the position of the icon, standard is left. options are: left, right, down
 *  @property [styling] [optional]
 *  a list of the type(s) and/or color(s) of the icon
 *  options are: large, medium, square, shadow, color-inherit, color-bright, disabled
 * @return {JSX.Element}
 */
export default function Icon(props) {
    let {name, position, styling} = props;

    if (!IconNames.includes(name)) {
        name = "times";
    }

    position = position === undefined ? "left" : position;
    styling = styling === undefined ? "" : styling.map(style => "icon--" + style).join(" ");


    return <span className={"icon icon--" + position + " " + styling + "icon--" + name}></span>
}

/**
 * All the icons available within DIAS
 */
const IconNames = [
    "user",
    "globe",
    "facebook",
    "building",
    "random",
    "plus",
    "minus",
    "location",
    "chevron-up",
    "chevron-down",
    "search",
    "ellipsis-v",
    "info-circle",
    "file",
    "spinner",
    "check",
    "user-tie",
    "ban",
    "linkedin",
    "instagram",
    "times",
    "info",
    "exclamation",
    "chevron-left",
    "exclamation-triangle",
    "chevron-right",
    "pen",
    "angle-down",
    "angle-up",
    "folders",
    "folder",
    "users",
    "angle-left",
    "angle-right",
    "sliders-h",
    "file-plus",
    "file-chart-line",
    "folder-plus",
    "save",
    "print",
    "file-export",
    "trash-alt",
    "chart-network",
    "link",
    "archive",
    "arrow-right",
    "layer-plus",
    "map-marker-alt",
    "chart-bar",
    "mask",
    "list-ul",
    "phone-alt",
    "power-off",
    "rectangle-landscape",
    "bars",
    "arrow-to-left",
    "arrow-to-right",
    "archive-arrow-down",
    "archive-arrow-up",
    "arrow-to-bottom",
    "file-csv",
    "map",
    "graph",
    "twitter",
    "user-secret",
    "home",
    "envelope",
    "money-check-edit-alt",
    "calendar-plus",
    "siren-on",
    "tag",
    "external-link-alt",
    "radar",
    "balance-scale",
    "sign",
    "image",
    "pen-nib",
    "clock",
    "star",
    "address-card",
    "comment-alt-exclamation",
    "comment-alt-dots",
    "image-alt",
    "calendar-alt",
    "play",
    "pause",
    "hourglass-full",
    "hourglass-empty",
    "file-alt",
    "adjust",
    "dna",
    "database",
    "tree-palm",
    "checkbox",
    "arrows",
    "bookmark",
    "bookmark-o",
    "file-code",
    "thumbs-up",
    "map-marker-check",
    "sticky-note",
    "camera",
    "file-upload",
    "circle",
    "copy",
    "keyboard",
    "cog",
    "star-half-alt",
    "star-empty",
    "eye",
    "google-account",
    "google-places",
    "review",
    "building-info",
    "galaxy"]