import React from "react";

/**
 *
 * @property title the header title of the modal
 * @property headerIcon the header icon of the modal
 * @property modalClasses styling of the modal as a whole. Options are:  flex-container--shrink (it shrinks the modal to fit the content)
 * @property body_content a JSX.Element that contains the content in the modal. Example: <div><h1>Apertif</h1></div>
 * @property footer_content a JSX.Element that contains the footer in the modal.
 * @property show a boolean that indicates whether or not the modal is visible
 * @return {JSX.Element}
 * @constructor
 */
export default function Modal(props) {
    const {modalClasses, headerIcon, title, content, footer, show, onClose} = props;

    if (!show) {
        return null;
    }

    return <div className="overlay">
        <div className="modal-dias-wrapper">
            <div className={"modal-dias " + modalClasses}>
                <div className="icon icon--times button--close" onClick={onClose}></div>
                <header className="modal-dias__header">
                    <h2 className="modal-dias__title">
                        <span class={"icon icon--medium icon--down icon--" + headerIcon}>
                        </span>{title}
                    </h2>
                </header>
                <div className="modal-dias__content">
                    {content}
                </div>
                <div className="modal-dias__footer">
                    {footer}
                </div>
            </div>
        </div>
    </div>
}