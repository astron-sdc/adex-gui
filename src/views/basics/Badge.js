import React from "react";

/**
 * @property message the message to display in the badge (can also be a number)
 * @property isCounter [optional] indicates whether it needs styling regarding a number
 * @property isLink [optional] indicates whether it needs the container and styling regarding a link
 * @property href [optional] if it is a link, it needs the go to url
 * @property isLarge [optional] the size of the badge if it's larger
 * @return {JSX.Element}
 * @constructor
 */
export default function Badge(props) {
    const {message, isCounter, isLink, href, isLarge} = props;

    let className = "badge " + (isCounter ? "badge--counter" : "");
    className = className + (isLarge === undefined ? "" : " badge--large");

    if (isLink && href !== undefined) {
        return <a className={className} href={href}>{message}</a>;
    }

    return <div className={className}>{message}</div>;
}